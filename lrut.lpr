program lrut;

{$mode objfpc}{$H+}

uses {$IFDEF UNIX} {$IFDEF UseCThreads}
  cthreads, {$ENDIF} {$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms,
  main,
  LResources,
  lazdbexport,
  lazreportpdfexport,
  sqldb,
  frm_sql,
  frm_database,
  frm_report,
  frm_grid,
  frm_conf,
  frm_fieldlist,
  frm_dbitems,
  frm_workzone,
  datamodule,
  about,
  manager,
  dummy_workzone,
  dummy_conf,
  dummy_database,
  dummy_dbitems,
  dummy_fieldlist,
  dummy_grid,
  dummy_report,
  dummy_sql,
  allforms;

{$R lrut.res}

begin
  Application.Title := 'LRUT';
  Application.Initialize;
  Application.CreateForm(TLRUTDataModule, LRUTDataModule);
  Application.CreateForm(TMForm, MForm);
  Application.CreateForm(TAboutFrm, AboutFrm);
  Application.Run;
end.

