{
 *****************************************************************************
 *                                                                           *
 *  datamodule.pas, This file is part of the LazReport Unit Test (LRUT)      *
 *                                                                           *
 *  See the file gpl.txt, included in this distribution, for details         *
 *  about the copyright.                                                     *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
}

unit datamodule;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, sqldb, DB, FileUtil, LResources, Forms, Controls, Dialogs,
  SynHighlighterSQL, LR_Desgn, LR_PGrid, LR_Class, LR_DBSet, LR_BarC, lr_e_pdf,

  // LRUT units
  connection, sqlds, lrut_report, metadata;

type

  { TLRUTDataModule }

  TLRUTDataModule = class(TDataModule)
    codigoBarras: TfrBarCodeObject;
    ILApp:      TImageList;
    Informe:    TfrReport;
    ODDatabase: TOpenDialog;
    ODOpenSQL:  TOpenDialog;
    PDFExporter: TfrTNPDFExport;
    qTransaction: TSQLTransaction;
    rptDataSet: TfrDBDataSet;
    rptDataSource: TDatasource;
    rptDesigner: TfrDesigner;
    rptPrintGrid: TFrPrintGrid;
    rptQuery:   TSQLQuery;
    SDSaveSQL:  TSaveDialog;
    SSSSQL:     TSynSQLSyn;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }


    con: TConexion;
    sql: TSQL;
    inf: TInforme;
    md:  TMetaData;
    acd: string;
  end;

var
  LRUTDataModule: TLRUTDataModule;


implementation

{ TLRUTDataModule }

procedure TLRUTDataModule.DataModuleCreate(Sender: TObject);
begin
  con := TConexion.Create;
  sql := TSQL.Create;
  inf := TInforme.Create;
  md  := TMetaData.Create(LRUTDataModule);
end;

procedure TLRUTDataModule.DataModuleDestroy(Sender: TObject);
begin
  rptQuery.Active := False;
  FreeAndNil(con);
  FreeAndnil(sql);
  FreeAndNil(inf);
  FreeAndNil(md);
end;

initialization
  {$I datamodule.lrs}

end.

