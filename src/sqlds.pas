{
 *****************************************************************************
 *                                                                           *
 *  sqlds.pas, This file is part of the LazReport Unit Test (LRUT)           *
 *                                                                           *
 *  See the file gpl.txt, included in this distribution, for details         *
 *  about the copyright.                                                     *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
}

unit sqlds;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LRUTgaSQLParser, sqldb;

type
  { TSQL }
  TSQL = class(TObject)
  private
    FArchivo: string;
    FDescripcion: string;
    FNConexion: string;
    FSQLParser: TLRUTgaSQLParser;
    FQuery: TSQLQuery;
    FTransaction: TSQLTransaction;

    function GetArchivo: string;
    procedure SetArchivo(Value: string);
    function GetDescripcion: string;
    procedure SetDescripcion(Value: string);
    function GetNConexion: string;
    procedure SetNConexion(Value: string);
    function GetSQLParser: TLRUTgaSQLParser;
    procedure SetSQLParser(Value: TLRUTgaSQLParser);
    function GetQuery: TSQLQuery;
    function GetTransaction: TSQLTransaction;
    procedure SetTransaction(Value: TSQLTransaction);


  public
    constructor Create;
    destructor Destroy; override;

  published
    property Archivo: string Read GetArchivo Write SetArchivo;
    property Descripcion: string Read GetDescripcion Write SetDescripcion;
    property NConexion: string Read GetNConexion Write SetNConexion;
    property SQLParser: TLRUTgaSQLParser Read GetSQLParser Write SetSQLParser;
    property Query: TSQLQuery Read GetQuery;
    property Transaction: TSQLTransaction Read GetTransaction Write SetTransaction;

  end;

implementation

{ TSQL }
constructor TSQL.Create;
begin
  FArchivo    := '';
  FDescripcion := '';
  FNConexion  := '';
  FSQLParser  := TLRUTgaSQLParser.Create(nil);
  FQuery      := TSQLQuery.Create(nil);
  FQuery.Name := 'TSQL_FBQuery';
  FTransaction := TSQLTransaction.Create(nil);
  FTransaction.Name := 'TSQL_FTransaction';
  FQuery.Transaction := FTransaction;
end;

destructor TSQL.Destroy;
begin
  FSQLParser.Destroy;
  FQuery.Active := False;
  FQuery.Destroy;
  FTransaction.Destroy;
  inherited Destroy;
end;

function TSQL.GetArchivo: string;
begin
  Result := FArchivo;
end;

procedure TSQL.SetArchivo(Value: string);
begin
  FArchivo := Value;
end;

function TSQL.GetDescripcion: string;
begin
  Result := FDescripcion;
end;

procedure TSQL.SetDescripcion(Value: string);
begin
  FDescripcion := Value;
end;

function TSQL.GetNConexion: string;
begin
  Result := FNConexion;
end;

procedure TSQL.SetNConexion(Value: string);
begin
  FNConexion := Value;
end;

function TSQL.GetSQLParser: TLRUTgaSQLParser;
begin
  Result := FSQLParser;
end;

procedure TSQL.SetSQLParser(Value: TLRUTgaSQLParser);
begin
  FSQLParser := Value;
end;

function TSQL.GetQuery: TSQLQuery;
begin
  Result := FQuery;
end;

function TSQL.GetTransaction: TSQLTransaction;
begin
  Result := FTransaction;
end;

procedure TSQL.SetTransaction(Value: TSQLTransaction);
begin
  FTransaction := Value;
end;

end.

