{
 *****************************************************************************
 *                                                                           *
 *  connection.pas, This file is part of the LazReport Unit Test (LRUT)      *
 *                                                                           *
 *  See the file gpl.txt, included in this distribution, for details         *
 *  about the copyright.                                                     *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
}

unit connection;

{$mode objfpc}{$H+}

interface

uses
  sqldb, LCLProc, Classes, Dialogs, SysUtils, IBConnection, mysql40conn, mysql41conn,
  mysql50conn, pqconnection, OracleConnection, odbcconn, sqlite3conn;

type
  { TConexion }
  TConexion = class(TObject)
  private
    FBQuery:      TSQLQuery;
    FConnection:  TSQLConnection;
    FConnectionName: string;
    FDBType:      integer;
    FQTransaction: TSQLTransaction;
    FSavePassword: boolean;
    FTransaction: TSQLTransaction;

    function GetConnectionName: string;
    function GetDatabaseName: string;
    function GetDBType: integer;
    function GetHostName: string;
    function GetPassword: string;
    function GetSavePassword: boolean;
    function GetSQLConnection: TSQLConnection;
    function GetUserName: string;
    procedure SetConnectionName(Value: string);
    procedure SetDatabaseName(Value: string);
    procedure SetDBType(Value: integer);
    procedure SetHostName(Value: string);
    procedure SetPassword(Value: string);
    procedure SetSavePassword(Value: boolean);
    procedure SetUserName(Value: string);
    procedure WireDatasetControls;

  protected

  public
    constructor Create;
    destructor Destroy; override;
    procedure SetConnectionDBType(Value: integer);

  published
    property ConnectionName: string Read GetConnectionName Write SetConnectionName;
    property DatabaseName: string Read GetDatabaseName Write SetDatabaseName;
    property DBType: integer Read GetDBType Write SetDBType;
    property HostName: string Read GetHostName Write SetHostName;
    property Password: string Read GetPassword Write SetPassword;
    property SavePassword: boolean Read GetSavePassword Write SetSavePassword;
    property SQLConnection: TSQLConnection Read GetSQLConnection;
    property UserName: string Read GetUserName Write SetUserName;

  end;

implementation

{ TConexion }

constructor TConexion.Create;
begin
  FConnection  := TSQLConnection.Create(nil);
  FTransaction := TSQLTransaction.Create(nil);
  FTransaction.Name := 'TConexion_FTransaction';
  FBQuery      := TSQLQuery.Create(nil);
  FBQuery.Name := 'TConexion_FBQuery';
  FQTransaction := TSQLTransaction.Create(nil);
  FQTransaction.Name := 'TConexion_FQTransaction';

  DBType := -1;
  savePassword := False;
end;

destructor TConexion.Destroy;
begin
  FQTransaction.Destroy;
  FBQuery.Active := False;
  FBQuery.Destroy;
  FTransaction.Destroy;
  FConnection.Destroy;
  inherited Destroy;
end;

function TConexion.GetPassword: string;
begin
  Result := FConnection.Password;
end;

procedure TConexion.SetPassword(Value: string);
begin
  FConnection.Password := Value;
end;

function TConexion.GetUserName: string;
begin
  Result := FConnection.UserName;
end;

procedure TConexion.SetUserName(Value: string);
begin
  FConnection.UserName := Value;
end;

function TConexion.GetHostName: string;
begin
  Result := FConnection.HostName;
end;

procedure TConexion.SetHostName(Value: string);
begin
  FConnection.HostName := Value;
end;

function TConexion.GetDatabaseName: string;
begin
  Result := FConnection.DatabaseName;
end;

procedure TConexion.SetDatabaseName(Value: string);
begin
  FConnection.DatabaseName := Value;
end;

function TConexion.GetSQLConnection: TSQLConnection;
begin
  Result := FConnection;
end;

function TConexion.GetConnectionName: string;
begin
  Result := FConnectionName;
end;

procedure TConexion.SetConnectionName(Value: string);
begin
  FConnectionName := Value;
end;

function TConexion.GetSavePassword: boolean;
begin
  Result := FSavePassword;
end;

procedure TConexion.SetSavePassword(Value: boolean);
begin
  FSavePassword := Value;
end;

function TConexion.GetDBType: integer;
begin
  Result := FDBType;
end;

procedure TConexion.SetDBType(Value: integer);
begin
  FDBType := Value;
end;

procedure TConexion.WireDatasetControls;
begin
  FQTransaction.DataBase := FConnection;
  FBQuery.Transaction := FQTransaction;
  FBQuery.DataBase := FConnection;
  FConnection.Transaction := FTransaction;
end;

procedure TConexion.SetConnectionDBType(Value: integer);
var
  uTmp, pTmp, DBTmp, hTmp: string;
  firebirdC: TIBConnection;
  MySQL40C: TMySQL40Connection;
  MySQL41C: TMySQL41Connection;
  MySQL50C: TMySQL50Connection;
  ODBCC:    TODBCConnection;
  OracleC:  TOracleConnection;
  PQC:      TPQConnection;
  SQLite3C: TSQLite3Connection;
  OldConnection: TSQLConnection;
begin

  FDbType := Value;

  if FConnection <> nil then
  begin
    uTmp  := FConnection.UserName;
    hTmp  := FConnection.HostName;
    DBTmp := FConnection.DatabaseName;
    pTmp  := FConnection.Password;
  end;
  OldConnection := FConnection;
  if FConnection <> nil then
    FreeAndNil(FConnection);

  case Value of
    0:
    begin
      firebirdC   := TIBConnection.Create(nil);
      FConnection := firebirdC;
      FConnection.CharSet := 'UTF8';
    end;
    1:
    begin
      MySQL40C    := TMySQL40Connection.Create(nil);
      FConnection := MySQL40C;
    end;
    2:
    begin
      MySQL41C    := TMySQL41Connection.Create(nil);
      FConnection := MySQL41C;
    end;
    3:
    begin
      MySQL50C    := TMySQL50Connection.Create(nil);
      FConnection := MySQL50C;
    end;
    4:
    begin
      PQC := TPQConnection.Create(nil);
      FConnection := PQC;
    end;
    5:
    begin
      OracleC     := TOracleConnection.Create(nil);
      FConnection := OracleC;
    end;
    6:
    begin
      ODBCC := TODBCConnection.Create(nil);
      FConnection := ODBCC;
    end;
    7:
    begin
      SQLite3C    := TSQLite3Connection.Create(nil);
      FConnection := SQLite3C;
    end;
  end;

  if (FConnection <> nil) and (OldConnection <> nil) then
  begin
    FConnection.UserName     := uTmp;
    FConnection.HostName     := hTmp;
    FConnection.DatabaseName := DBTmp;
    FConnection.Password     := pTmp;
    WireDatasetControls;
  end;
end;

end.

