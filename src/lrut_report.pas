{
 *****************************************************************************
 *                                                                           *
 *  lrut_report.pas This file is part of the LazReport Unit Test (LRUT)           *
 *                                                                           *
 *  See the file gpl.txt, included in this distribution, for details         *
 *  about the copyright.                                                     *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
}

unit lrut_report;

{$mode objfpc}{$H+}

interface

uses
  Classes;

type
  { TInforme }
  TInforme = class(TObject)
  public
    archivo, descripcion, nsql: string;
    constructor Create;
  private
  end;

implementation

{ TInforme }
constructor TInforme.Create;
begin
  archivo := '';
  descripcion := '';
  nsql := '';
end;

end.

