{
 *****************************************************************************
 *                                                                           *
 *  MetaData.pas, This file is part of the LazReport Unit Test (LRUT)        *
 *                                                                           *
 *  See the file gpl.txt, included in this distribution, for details         *
 *  about the copyright.                                                     *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
}

unit MetaData;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, sqldb, LCLProc, IBConnection, mysql40conn, mysql41conn,
  mysql50conn, pqconnection, OracleConnection, odbcconn, sqlite3conn, Dialogs,
  dbugintf;

type

  { TMetaData }
  TMetaData = class(TComponent)
  private
    FConnection: TSQLConnection;
    FTransaction: TSQLTransaction;
    FQuery:  TSQLQuery;
    FQTransaction: TSQLTransaction;
    FDBType: integer;

    procedure SetConnection(Value: TSQLConnection);
    function GetSchemaInfoSQL(SchemaType: TSchemaType;
      SchemaObjectName, SchemaPattern: string): string;
    function GetMySQLSchemaInfoSQL(SchemaType: TSchemaType;
      SchemaObjectName, SchemaPattern: string): string;
    procedure GetMySQLFields(table: string; const AList: TStrings);
    procedure GetSQLite3Fields(table: string; const AList: TStrings);
    procedure GetFirebirdKeyFields(table: string; AList: TStrings);
    procedure GetMySQLKeyFields(table: string; const AList: TStrings);
    procedure GetSQLite3KeyFields(table: string; const AList: TStrings);
    function IsFieldOnTable(field, table: string): boolean;

  public
    constructor Create(AOwner: TComponent); override;
    procedure GetTableNames(List: TStrings);
    procedure GetFieldNames(const TableName: string; List: TStrings);
    procedure GetFieldsFromTable(table: string; const AList: TStrings);
    function GetFieldType(table: string; field: string): string;
    function GetFieldTypeName(table: string; field: string): string;
    function GetFieldLength(table: string; field: string): string;
    procedure GetKeyFieldsFromTable(table: string; const AList: TStrings);
    function IsKeyField(table: string; field: string): boolean;
    function SearchUniqueTable(tables: TStrings; fieldname: string): string;

  published
    property Connection: TSQLConnection Read FConnection Write SetConnection;

  end;

  { TFirebirdMD }
  TFirebirdMD = class(TIBConnection)
  private
  protected
  public
    constructor Create(AOwner: TComponent); override;
    function GetSQL(SchemaType: TSchemaType;
      SchemaObjectName, SchemaPattern: string): string;
  end;

  { TODBCMD }
  TODBCMD = class(TODBCConnection)
  private
  protected
  public
    constructor Create(AOwner: TComponent); override;
    function GetSQL(SchemaType: TSchemaType;
      SchemaObjectName, SchemaPattern: string): string;
  end;

  { TOracleMD }
  TOracleMD = class(TOracleConnection)
  private
  protected
  public
    constructor Create(AOwner: TComponent); override;
    function GetSQL(SchemaType: TSchemaType;
      SchemaObjectName, SchemaPattern: string): string;
  end;

  { TPQMD }
  TPQMD = class(TPQConnection)
  private
  protected
  public
    constructor Create(AOwner: TComponent); override;
    function GetSQL(SchemaType: TSchemaType;
      SchemaObjectName, SchemaPattern: string): string;
  end;

  { TSQLite3MD }
  TSQLite3MD = class(TSQLite3Connection)
  private
  protected
  public
    constructor Create(AOwner: TComponent); override;
    function GetSQL(SchemaType: TSchemaType;
      SchemaObjectName, SchemaPattern: string): string;
  end;

implementation

resourcestring
  rsNotTested = 'Not tested/implemented yet!';

{ TFirebirdMD }
constructor TFirebirdMD.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

function TFirebirdMD.GetSQL(SchemaType: TSchemaType;
  SchemaObjectName, SchemaPattern: string): string;
begin
  // Asi no hace falta aplicar el patch a ibconnection
  case SchemaType of
    stProcedures, stSysTables, stTables: Result :=
        self.GetSchemaInfoSQL(SchemaType, SchemaObjectName, SchemaPattern);
    stColumns: Result := 'select ' + 'rf.rdb$field_id         as recno, ' +
        '''' + DatabaseName + ''' as catalog_name, ' +
        '''''                     as schema_name, ' +
        'rf.rdb$relation_name     as table_name, ' +
        'rf.rdb$field_name        as column_name, ' +
        'rf.rdb$field_position    as column_position, ' +
        'f.RDB$FIELD_TYPE         as column_type, ' +
        '0                        as column_datatype, ' +
        '''''                     as column_typename, ' +
        'f.RDB$FIELD_SUB_TYPE     as column_subtype, ' +
        'f.RDB$FIELD_PRECISION    as column_precision, ' +
        'f.RDB$FIELD_SCALE        as column_scale, ' +
        'f.RDB$FIELD_LENGTH       as column_length, ' +
        'f.RDB$NULL_FLAG          as column_nullable ' + 'from ' +
        'rdb$relation_fields rf JOIN RDB$FIELDS f ON rf.RDB$FIELD_source=f.RDB$FIELD_NAME '
        + 'WHERE ' +
        '(rf.rdb$system_flag = 0 or rf.rdb$system_flag is null) and (rf.rdb$relation_name = '''
        +
        Uppercase(SchemaObjectName) + ''') ' + 'order by rf.rdb$field_name';
  end;
end;

{ TODBCMD }
constructor TODBCMD.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

function TODBCMD.GetSQL(SchemaType: TSchemaType;
  SchemaObjectName, SchemaPattern: string): string;
begin
  Result := self.GetSchemaInfoSQL(SchemaType, SchemaObjectName, SchemaPattern);
end;

{ TOracleMD }
constructor TOracleMD.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

function TOracleMD.GetSQL(SchemaType: TSchemaType;
  SchemaObjectName, SchemaPattern: string): string;
begin
  Result := self.GetSchemaInfoSQL(SchemaType, SchemaObjectName, SchemaPattern);
end;

{ TPQMD }
constructor TPQMD.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

function TPQMD.GetSQL(SchemaType: TSchemaType;
  SchemaObjectName, SchemaPattern: string): string;
begin
  Result := self.GetSchemaInfoSQL(SchemaType, SchemaObjectName, SchemaPattern);
end;

{ TSQLite3MD }
constructor TSQLite3MD.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

function TSQLite3MD.GetSQL(SchemaType: TSchemaType;
  SchemaObjectName, SchemaPattern: string): string;
begin
  Result := self.GetSchemaInfoSQL(SchemaType, SchemaObjectName, SchemaPattern);
end;

{ TMetaData }
constructor TMetaData.Create(AOwner: TComponent);
begin
  FConnection := TSQLConnection.Create(AOwner);
  FTransaction := TSQLTransaction.Create(FConnection);
  FTransaction.Name := 'TMetadata_FTransaction';
  FQuery      := TSQLQuery.Create(FConnection);
  FQuery.Name := 'TMetadata_FQuery';
  FQTransaction := TSQLTransaction.Create(FConnection);
  FQTransaction.Name := 'TMetadata_FQTransaction';
  FDBType     := -1;
end;

procedure TMetaData.SetConnection(Value: TSQLConnection);
begin
  if Value.ClassNameIs('TIBConnection') then
    FDBType := 0;
  if Value.ClassNameIs('TMySQL40Connection') then
    FDBType := 1;
  if Value.ClassNameIs('TMySQL41Connection') then
    FDBType := 2;
  if Value.ClassNameIs('TMySQL50Connection') then
    FDBType := 3;
  if Value.ClassNameIs('TPQConnection') then
    FDBType := 4;
  if Value.ClassNameIs('TOracleConnection') then
    FDBType := 5;
  if Value.ClassNameIs('TODBCConnection') then
    FDBType := 6;
  if Value.ClassNameIs('TSQLite3Connection') then
    FDBType := 7;

  FConnection := Value;
  FConnection.Transaction := FTransaction;

  FQuery.DataBase    := FConnection;
  FQuery.Transaction := FQTransaction;

  FQTransaction.DataBase := FConnection;
end;

procedure TMetaData.GetTableNames(List: TStrings);
begin
  FConnection.GetTableNames(List, False);
end;

procedure TMetaData.GetFieldNames(const TableName: string; List: TStrings);
begin
  FConnection.GetFieldNames(TableName, List);
end;

procedure TMetaData.GetFieldsFromTable(table: string; const AList: TStrings);
begin
  case FDBType of
    0: FConnection.GetFieldNames(table, AList); // Firebird
    1: GetMySQLFields(table, AList);            // MySQL40
    2: GetMySQLFields(table, AList);            // MySQL41
    3: GetMySQLFields(table, AList);            // MySQL50
    4: GetFieldNames(table, AList); // PostgreSQL
    5: ShowMessage(rsNotTested);                             // Oracle
    6: ShowMessage(rsNotTested);                             // ODBC
    7: GetSQLite3Fields(table, AList);          // SQLite3
  end;
end;

procedure TMetaData.GetKeyFieldsFromTable(table: string; const AList: TStrings);
begin
  case FDBType of
    0: GetFirebirdKeyFields(table, AList);   // Firebird
    1: GetMySQLKeyFields(table, AList);      // MySQL40
    2: GetMySQLKeyFields(table, AList);      // MySQL41
    3: GetMySQLKeyFields(table, AList);      // MySQL50
    4: ShowMessage(rsNotTested);                             // PostgreSQL
    5: ShowMessage(rsNotTested);                             // Oracle
    6: ShowMessage(rsNotTested);                             // ODBC
    7: GetSQLite3KeyFields(table, AList);    // SQLite3
  end;
end;

procedure TMetaData.GetMySQLFields(table: string; const AList: TStrings);
begin
  FQuery.Active := False;
  FQuery.SQL.Clear;
  FQuery.SQL.Append('SELECT column_name ');
  FQuery.SQL.Append('FROM information_schema.`COLUMNS` ');
  FQuery.SQL.Append('where table_name =' + QuotedStr(table));
  FQuery.Active := True;

  FQuery.First;

  while not (FQuery.EOF) do
  begin
    AList.Append(FQuery.FieldByName('column_name').AsString);
    FQuery.Next;
  end;
end;

procedure TMetaData.GetSQLite3Fields(table: string; const AList: TStrings);
begin
  FQuery.Active := False;
  FQuery.SQL.Clear;
  FQuery.SQL.Append('pragma table_info(' + trim(table) + ')');
  FQuery.Active := True;
  FQuery.First;

  while not (FQuery.EOF) do
  begin
    AList.Append(FQuery.Fields[1].AsString);
    FQuery.Next;
  end;
end;

function TMetaData.GetSchemaInfoSQL(SchemaType: TSchemaType;
  SchemaObjectName, SchemaPattern: string): string;
var
  FirebirdMD: TFirebirdMD;
  ODBCMD:    TODBCMD;
  OracleMD:  TOracleMD;
  PQMD:      TPQMD;
  SQLite3MD: TSQLite3MD;
begin
  case FDBType of
    0:
    begin
      FirebirdMD := TFirebirdMD.Create(nil);
      Result     := FirebirdMD.GetSQL(SchemaType, SchemaObjectName, SchemaPattern);
      FirebirdMD.Free;
    end;
    1, 2, 3:
    begin
      Result := GetMySQLSchemaInfoSQL(SchemaType, SchemaObjectName, SchemaPattern);
    end;
    4:
    begin
      ODBCMD := TODBCMD.Create(nil);
      Result := ODBCMD.GetSQL(SchemaType, SchemaObjectName, SchemaPattern);
      ODBCMD.Free;
    end;
    5:
    begin
      OracleMD := TOracleMD.Create(nil);
      Result   := OracleMD.GetSQL(SchemaType, SchemaObjectName, SchemaPattern);
      OracleMD.Free;
    end;
    6:
    begin
      PQMD   := TPQMD.Create(nil);
      Result := PQMD.GetSQL(SchemaType, SchemaObjectName, SchemaPattern);
      PQMD.Free;
    end;
    7:
    begin
      SQLite3MD := TSQLite3MD.Create(nil);
      Result    := SQLite3MD.GetSQL(SchemaType, SchemaObjectName, SchemaPattern);
      SQLite3MD.Free;
    end;
  end;
end;

function TMetaData.GetFieldTypeName(table: string; field: string): string;
var
  typeTmp: string;
begin
  Result  := 'Unknown';
  typeTmp := GetFieldType(table, field);

  if FConnection.ClassNameIs('TIBConnection') then
    case StrToInt(Trim(typeTmp)) of
      7, 8, 9, 16: Result := 'Integer';
      10, 27: Result  := 'Float';
      12: Result      := 'Date';
      13: Result      := 'Time';
      14: Result      := 'Text';
      35: Result      := 'DateTime';
      37: Result      := 'Varchar';
      45, 261: Result := 'Blob';
    end;

  if FConnection.ClassNameIs('TMySQL40Connection') or
    FConnection.ClassNameIs('TMySQL41Connection') or
    FConnection.ClassNameIs('TMySQL50Connection') then
    Result := typeTmp;
end;

function TMetaData.GetFieldLength(table: string; field: string): string;
var
  tmpStr: string;
begin
  FQuery.Active := False;
  FQuery.SQL.Clear;
  FQuery.SQL.Append(Self.GetSchemaInfoSQL(stColumns, table, tmpStr));
  FQuery.Active := True;
  FQuery.First;

  while not (FQuery.EOF) do
  begin
    if Trim(FQuery.FieldByName('column_name').AsString) = Trim(field) then
    begin
      Result := FQuery.FieldByName('column_length').AsString;
      exit;
    end;
    FQuery.Next;
  end;
end;

function TMetaData.GetFieldType(table: string; field: string): string;
var
  tmpStr: string;
begin
  FQuery.Active := False;
  FQuery.SQL.Clear;
  FQuery.SQL.Append(Self.GetSchemaInfoSQL(stColumns, table, tmpStr));
  FQuery.Active := True;
  FQuery.First;

  while not (FQuery.EOF) do
  begin
    if Trim(FQuery.FieldByName('column_name').AsString) = Trim(field) then
    begin
      Result := FQuery.FieldByName('column_type').AsString;
      exit;
    end;
    FQuery.Next;
  end;
end;

function TMetaData.GetMySQLSchemaInfoSQL(SchemaType: TSchemaType;
  SchemaObjectName, SchemaPattern: string): string;
var
  s: string;
begin
  case SchemaType of
    stTables: s    := 'select ' + '0 as recno, ' +
        'table_schema   as catalog_name, ' + 'table_schema   as schema_name, ' +
        'table_name     as table_name, ' + 'table_type     as table_type ' +
        'from ' + 'information_schema.tables ' + 'where table_schema=' +
        QuotedStr(SchemaObjectName) + ' ' + 'order by table_name';
    stSysTables: s := 'select ' + '0 as recno, ' +
        'table_schema   as catalog_name, ' + 'table_schema   as schema_name, ' +
        'table_name     as table_name, ' + 'table_type     as table_type ' +
        'from ' + 'information_schema.tables ' +
        'where table_schema=''mysql'' or table_schema=''information_schema'' ' +
        'order by table_name';
    stColumns: s   := 'select ' + 'ordinal_position           as recno, ' +
        'table_schema               as catalog_name, ' +
        'table_schema               as schema_name, ' +
        'table_name                 as table_name, ' +
        'column_name                as column_name, ' +
        'ordinal_position           as column_position, ' +
        'data_type                  as column_type, ' +
        '0                          as column_datatype, ' +
        '''''                       as column_typename, ' +
        '0                          as column_subtype, ' +
        'numeric_PRECISION          as column_precision, ' +
        '0                          as column_scale, ' +
        'character_maximum_length    as column_length, ' +
        'is_nullable                as column_nullable ' + 'from ' +
        'information_schema.columns ' + 'WHERE ' + 'table_name= ' +
        QuotedStr(SchemaObjectName) + ' ' + 'order by column_name';
  end;
  Result := s;
end;

procedure TMetaData.GetFirebirdKeyFields(table: string; AList: TStrings);
begin
  FQuery.Active := False;
  FQuery.SQL.Clear;
  FQuery.SQL.Append('select i.RDB$FIELD_NAME as keyfields ');
  FQuery.SQL.Append(
    'from rdb$relation_constraints rc, rdb$index_segments i, rdb$indices idx ');
  FQuery.SQL.Append(
    'where (i.rdb$index_name = rc.rdb$index_name) and (idx.rdb$index_name = rc.rdb$index_name) and (rc.rdb$constraint_type = ''PRIMARY KEY'') and (rc.rdb$relation_name = ' + QuotedStr(UpperCAse(table)) + ') ');
  FQuery.SQL.Append('order by i.rdb$field_position  ');
  FQuery.Active := True;
  FQuery.First;

  while not (FQuery.EOF) do
  begin
    AList.Append(Trim(FQuery.FieldByName('keyfields').AsString));
    FQuery.Next;
  end;
end;

procedure TMetaData.GetMySQLKeyFields(table: string; const AList: TStrings);
begin
  FQuery.Active := False;
  FQuery.SQL.Clear;
  FQuery.SQL.Append('SELECT column_name as keyfields ');
  FQuery.SQL.Append('FROM information_schema.KEY_COLUMN_USAGE ');
  FQuery.SQL.Append('where constraint_schema=' + QuotedStr(
    FConnection.DatabaseName) + ' and table_name=' + QuotedStr(table));
  FQuery.Active := True;
  FQuery.First;

  while not (FQuery.EOF) do
  begin
    AList.Append(FQuery.FieldByName('keyfields').AsString);
    FQuery.Next;
  end;
end;

procedure TMetaData.GetSQLite3KeyFields(table: string; const AList: TStrings);
begin

end;

function TMetaData.IsKeyField(table: string; field: string): boolean;
var
  tmp: TStringList;
  i:   integer;
begin
  Result := False;
  tmp    := TStringList.Create;
  GetKeyFieldsFromTable(table, tmp);
  for i := 0 to tmp.Count - 1 do
    if UpperCase(field) = UpperCase(tmp[i]) then
    begin
      Result := True;
      break;
    end;
  tmp.Free;
end;

function TMetaData.SearchUniqueTable(tables: TStrings; fieldname: string): string;
var
  i, Count: integer;
begin
  if Self.Connection.Connected = False then
    exit;

  Count := 0;
  for i := 0 to tables.Count - 1 do
    if IsFieldOnTable(fieldname, tables[i]) then
    begin
      Inc(Count);
      Result := tables[i];
    end;

  if Count <> 1 then
    Result := '';
end;

function TMetaData.IsFieldOnTable(field, table: string): boolean;
var
  tmpFields: TStringList;
  i: integer;
begin
  tmpFields := TStringList.Create;
  GetFieldNames(table, tmpFields);

  for i := 0 to tmpFields.Count - 1 do
    if UpCase(Trim(field)) = UpCase(Trim(tmpFields[i])) then
    begin
      FreeAndNil(tmpFields);
      Result := True;
      exit;
    end;

  Result := False;
  FreeAndNil(tmpFields);
end;

end.

