{ Funciones LRUT}
procedure guardaDataSetArchivo(dsTmp : TDataSet; aTmp : String);
var
  exportador : TCSVExporter;
  formato: TCSVFormatSettings;
begin
  if FileExists(aTmp + '.bkp') then
    DeleteFile(aTmp + '.bkp');

  RenameFile(aTmp, aTmp + '.bkp');

  formato := TCSVFormatSettings.Create(true);
  formato.FieldDelimiter:=#09;
  formato.HeaderRow:=false;
  exportador := TCSVExporter.Create(nil);
  exportador.FormatSettings := formato;
  exportador.FromCurrent:=false;
  exportador.Dataset:=dsTmp;
  exportador.FileName:=aTmp;
  exportador.Execute;
  exportador.Free;
  formato.Free;
end;

procedure cargaDataSetArchivo(dsTmp : TBufDataset; aTmp : String);
var
  ArchivoTexto : TextFile;
  Cadena : String;
  i : Integer;
  PIni,PNext,PFin: PChar;
begin
  AssignFile(ArchivoTexto, aTmp );
  Reset(ArchivoTexto);

  while not eof(ArchivoTexto) do
    begin
      Readln(ArchivoTexto,Cadena);
      dsTmp.Append;
      PIni := @Cadena[1];
      PNext:= PIni;
      PFin := PIni + Length(Cadena);
      i := 0;
      while (PIni<PFin) and (PNext<>nil) do
        begin
          PNext := StrScan(PIni, #9);
          if PNext=nil then
            dsTmp.Fields[i].AsString := Pini
          else
            begin
              PNext^ := #0;
              dsTmp.Fields[i].AsString := Pini;
              PIni := PNext + 1;
            end;
          inc(i);
        end;
      dsTmp.Post;
    end;
  CloseFile(ArchivoTexto);
end;

