{
 *****************************************************************************
 *                                                                           *
 *  manager.pas, This file is part of the LazReport Unit Test (LRUT)          *
 *                                                                           *
 *  See the file gpl.txt, included in this distribution, for details         *
 *  about the copyright.                                                     *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
}

unit manager;

{$mode objfpc}{$H+}

interface

uses
  Classes, Controls, Menus, bufdataset, DB, SysUtils, LCLProc, fpcsvexport,
  sqldb,

  // LRUT Units
  lrut_report, connection, sqlds;

type

  { TManager }
  TManager = class(TObject)
  private
    FJoins:     TBufDataset;
    FReports:   TBufDataset;
    FConnections: TBufDataset;
    FSQLs:      TBufDataset;
    FSelectedFields: TBufDataset;
    FConexionDS: TDatasource;
    FRelacionDS: TDatasource;
    FInformeDS: TDatasource;
    FSqlDS:     TDatasource;
    FFieldsDS:  TDataSource;
    FSQLToWorkZone: boolean;

  public
    constructor Create;
    destructor Destroy; override;
    procedure LoadConfiguration(ADir: string);
    procedure SaveConfiguration(ADir: string);

    function LoadConnection(AConnectionName: string;
      const AConnection: TConexion): boolean;
    function LoadSQL(ASQLNAme: string; const ASQL: TSQL): boolean;
    function LoadReport(AReportName: string; const AReport: TInforme): boolean;

    procedure DeleteConnection(Checked: boolean);
    procedure DeleteSQL(ACOnnectionName: string; Checked: boolean);
    procedure DeleteReport(ASQLName: string; Checked: boolean);

    function ConnectionExists(AConnection: TConexion): boolean;
    function SQLExists(ASQL: TSQL): boolean;
    function ReportExists(AReport: TInforme): boolean;

    procedure AddConnection(AConnection: TConexion);
    procedure AddSQL(ASQL: TSQL);
    procedure AddReport(AReport: TInforme);

    procedure UpdatePasswordConnection(AConnectionName: string; ANewPass: string);

    function AddJoin(campo1: string; tabla1: string; campo2: string;
      tabla2: string; joinType: string): boolean;

    procedure AddField(const ATable, AField, AWhere, AAlias, AOrderBy,
      AOrderPosition: string);

  published
    property Connections: TBufDataset Read FConnections Write FConnections;
    property SQLs: TBufDataset Read FSQLs Write FSQLs;
    property Reports: TBufDataset Read FReports Write FReports;
    property Joins: TBufDataset Read FJoins Write FJoins;
    property SelectedFields: TBufDataset Read FSelectedFields Write FSelectedFields;
    property ConexionDS: TDataSource Read FConexionDS Write FConexionDS;
    property SqlDS: TDataSource Read FSqlDS Write FSqlDS;
    property InformeDS: TDataSource Read FInformeDS Write FInformeDS;
    property RelacionDS: TDataSource Read FRelacionDS Write FRelacionDS;
    property FieldsDS: TDataSource Read FFieldsDS Write FFieldsDS;
    property SQLToWorkZone: boolean Read FSQLToWorkZone Write FSQLToWorkZone;
  end;

var
  LRUTManager: TManager;

implementation

{$I funclrut.inc}

{ TManager }
constructor TManager.Create;
begin
  FConnections := TBufDataset.Create(nil);
  FConnections.FieldDefs.Add('nombre', ftString, 50);
  FConnections.FieldDefs.Add('usuario', ftString, 50);
  FConnections.FieldDefs.Add('host', ftString, 50);
  FConnections.FieldDefs.Add('tipo', ftString, 1);
  FConnections.FieldDefs.Add('bbdd', ftString, 50);
  FConnections.FieldDefs.Add('password', ftString, 50);
  FConnections.FieldDefs.Add('guarda', ftBoolean);
  FConnections.CreateDataset;
  FConnections.Active := True;

  FSQLs := TBufDataset.Create(nil);
  FSQLs.FieldDefs.Add('archivo', ftString, 250);
  FSQLs.FieldDefs.Add('descripcion', ftString, 250);
  FSQLs.FieldDefs.Add('nconexion', ftString, 50);
  FSQLs.CreateDataset;
  FSQLs.Active := True;

  FReports := TBufDataset.Create(nil);
  FReports.FieldDefs.Add('archivo', ftString, 250);
  FReports.FieldDefs.Add('descripcion', ftString, 250);
  FReports.FieldDefs.Add('nsql', ftString, 250);
  FReports.CreateDataset;
  FReports.Active := True;

  FJoins := TBufDataset.Create(nil);
  FJoins.FieldDefs.Add('t1', ftString, 250);
  FJoins.FieldDefs.Add('c1', ftString, 250);
  FJoins.FieldDefs.Add('t2', ftString, 250);
  FJoins.FieldDefs.Add('c2', ftString, 250);
  FJoins.FieldDefs.Add('direccion', ftString, 100);
  FJoins.CreateDataset;
  FJoins.Active := True;

  FSelectedFields := TBufDataset.Create(nil);
  FSelectedFields.FieldDefs.Add('campo', ftString, 250);
  FSelectedFields.FieldDefs.Add('alias', ftString, 250);
  FSelectedFields.FieldDefs.Add('tabla', ftString, 250);
  FSelectedFields.FieldDefs.Add('resultado', ftBoolean);
  FSelectedFields.FieldDefs.Add('orden', ftString, 250);
  FSelectedFields.FieldDefs.Add('agrupar', ftString, 250);
  FSelectedFields.FieldDefs.Add('tipo', ftString, 250);
  FSelectedFields.FieldDefs.Add('filtro', ftString, 250);
  FSelectedFields.FieldDefs.Add('o', ftString, 250);
  FSelectedFields.CreateDataset;
  FSelectedFields.Active := True;

  FConexionDS := TDatasource.Create(nil);
  FRelacionDS := TDatasource.Create(nil);
  FInformeDS  := TDatasource.Create(nil);
  FSqlDS      := TDatasource.Create(nil);
  FFieldsDS   := TDatasource.Create(nil);

  FConexionDS.DataSet := FConnections;
  FRelacionDS.DataSet := FJoins;
  FInformeDS.DataSet  := FReports;
  FSqlDS.DataSet      := FSQLs;
  FFieldsDS.DataSet   := FSelectedFields;

  FSQLToWorkZone := False;
end;

destructor TManager.Destroy;
begin
  FJoins.Active := False;
  FJoins.Destroy;

  FReports.Active := False;
  FReports.Destroy;

  FSQLs.Active := False;
  FSQLs.Destroy;

  FConnections.Active := False;
  FConnections.Destroy;

  FSelectedFields.Active := False;
  FSelectedFields.Destroy;

  FSQLDS.Destroy;
  FInformeDS.Destroy;
  FRelacionDS.Destroy;
  FConexionDS.Destroy;
  FFieldsDS.Destroy;
end;

procedure TManager.LoadConfiguration(ADir: string);
begin
  if FileExists(ADir + 'conexiones.lrut') then
    cargaDataSetArchivo(FConnections, ADir + 'conexiones.lrut');

  if FileExists(ADir + 'sql.lrut') then
    cargaDataSetArchivo(FSQLs, ADir + 'sql.lrut');

  if FileExists(ADir + 'informes.lrut') then
    cargaDataSetArchivo(FReports, ADir + 'informes.lrut');
end;

procedure TManager.SaveConfiguration(ADir: string);
begin
  guardaDataSetArchivo(FConnections, ADir + DirectorySeparator + 'conexiones.lrut');
  guardaDataSetArchivo(FSQLs, ADir + DirectorySeparator + 'sql.lrut');
  guardaDataSetArchivo(FReports, ADir + DirectorySeparator + 'informes.lrut');
end;

function TManager.LoadConnection(AConnectionName: string;
  const AConnection: TConexion): boolean;
begin
  Result := FConnections.Locate('nombre', AConnectionName, [loCaseInsensitive]);

  if Result then
  begin
    AConnection.SetConnectionDBType(FConnections.FieldByName('tipo').AsInteger);
    AConnection.ConnectionName := FConnections.FieldByName('nombre').AsString;
    AConnection.UserName     := FConnections.FieldByName('usuario').AsString;
    AConnection.DatabaseName := FConnections.FieldByName('bbdd').AsString;
    AConnection.savePassword := FConnections.FieldByName('guarda').AsBoolean;
    AConnection.HostName     := FConnections.FieldByName('host').AsString;
    AConnection.Password     := FConnections.FieldByName('password').AsString;
  end;
end;

function TManager.LoadSQL(ASQLName: string; const ASQL: TSQL): boolean;
begin
  Result := FSQLs.Locate('archivo', ASQLName, [loCaseInsensitive]);

  if Result then
  begin
    ASQL.archivo     := FSQLs.FieldByName('archivo').AsString;
    ASQL.descripcion := FSQLs.FieldByName('descripcion').AsString;
    ASQL.nconexion   := FSQLs.FieldByName('nconexion').AsString;
  end;
end;

function TManager.LoadReport(AReportName: string; const AReport: TInforme): boolean;
begin
  Result := FReports.Locate('archivo', AReportName, [loCaseInsensitive]);

  if AReportName = FReports.FieldByName('archivo').AsString then
  begin
    AReport.archivo := FReports.FieldByName('archivo').AsString;
    AReport.descripcion := FReports.FieldByName('descripcion').AsString;
    AReport.nsql := FReports.FieldByName('nsql').AsString;
  end;
end;

procedure TManager.DeleteConnection(Checked: boolean);
begin
  if Checked then
    DeleteSQL(FConnections.FieldByName('nombre').AsString, Checked);
  FConnections.Delete;
end;

procedure TManager.DeleteSQL(AConnectionName: string; Checked: boolean);
begin
  if Checked then
  begin
    FSQLs.Last;
    while not (FSQLs.BOF) do
    begin
      if FSQLs.FieldByName('nconexion').AsString = AConnectionName then
      begin
        DeleteReport(FSQLs.FieldByName('archivo').AsString, Checked);
        FSQLs.Delete;
      end;
      FSQLs.Prior;
    end;
  end
  else
    FSQLs.Delete;
end;

procedure TManager.DeleteReport(ASQLName: string; Checked: boolean);
begin
  if Checked then
  begin
    FReports.Last;
    while not (FReports.BOF) do
    begin
      if FReports.FieldByName('nsql').AsString = ASQLName then
        FReports.Delete;
      FReports.Prior;
    end;
  end
  else
    FReports.Delete;
end;

function TManager.ConnectionExists(AConnection: TConexion): boolean;
begin
  Result := False;
  FConnections.First;
  while not (FConnections.EOF) do
    begin
      if (AConnection.UserName = FConnections.FieldByName('usuario').AsString) and
         (AConnection.HostName = FConnections.FieldByName('host').AsString) and
         (AConnection.DatabaseName = FConnections.FieldByName('bbdd').AsString) and
         (AConnection.DBType = FConnections.FieldByName('tipo').AsInteger) then
            begin
              Result := True;
              break;
            end;
      FConnections.Next;
    end;
end;

function TManager.SQLExists(ASQL: TSQL): boolean;
begin
  Result := FSQLs.Locate('archivo', ASQL.Archivo, [loCaseInsensitive]);
end;

function TManager.ReportExists(AReport: TInforme): boolean;
begin
  Result := FReports.Locate('archivo', AReport.Archivo, [loCaseInsensitive]);
end;

procedure TManager.AddConnection(AConnection: TConexion);
begin
  Connections.Append;
  Connections.FieldByName('nombre').AsString   := AConnection.ConnectionName;
  Connections.FieldByName('usuario').AsString  := AConnection.UserName;
  Connections.FieldByName('host').AsString     := AConnection.HostName;
  Connections.FieldByName('bbdd').AsString     := AConnection.DatabaseName;
  Connections.FieldByName('tipo').AsInteger    := AConnection.DBType;
  Connections.FieldByName('password').AsString := AConnection.Password;
  Connections.FieldByName('guarda').AsBoolean  := AConnection.savePassword;
  Connections.Post;
end;

procedure TManager.AddSQL(ASQL: TSQL);
begin
  SQLs.Append;
  SQLs.FieldByName('archivo').AsString     := ASQL.archivo;
  SQLs.FieldByName('descripcion').AsString := ASQL.descripcion;
  SQLs.FieldByName('nconexion').AsString   := ASQL.nconexion;
  SQLs.Post;
end;

procedure TManager.AddReport(AReport: TInforme);
begin
  Reports.Append;
  Reports.FieldByName('archivo').AsString := AReport.archivo;
  Reports.FieldByName('descripcion').AsString := AReport.descripcion;
  Reports.FieldByName('nsql').AsString := AReport.nsql;
  Reports.Post;
end;

procedure TManager.UpdatePasswordConnection(AConnectionName: string; ANewPass: string);
begin
  if FConnections.Locate('nombre', AConnectionName, [loCaseInsensitive]) then
  begin
    FConnections.Edit;
    FConnections.FieldByName('password').AsString := ANewPass;
    if not (ANewPass = '') then
      FConnections.FieldByName('guarda').AsBoolean := True;
    FConnections.Post;
  end;
end;

function TManager.AddJoin(campo1: string; tabla1: string; campo2: string;
  tabla2: string; joinType: string): boolean;
begin
  Result := True;

  // Comprobamos si la relacion existe
  Joins.First;
  while not (Joins.EOF) do
  begin
    if ((Joins.FieldByName('t1').AsString = tabla1) or
      (Joins.FieldByName('t1').AsString = tabla2)) and
      ((Joins.FieldByName('t2').AsString = tabla1) or
      (Joins.FieldByName('t2').AsString = tabla2)) and
      ((Joins.FieldByName('c1').AsString = campo1) or
      (Joins.FieldByName('c1').AsString = campo2)) and
      ((Joins.FieldByName('c2').AsString = campo1) or
      (Joins.FieldByName('c2').AsString = campo2)) then
    begin
      Result := False;
      exit;
    end;
    Joins.Next;
  end;

  if AddJoin then
  begin
    Joins.Append;
    Joins.FieldByName('c1').AsString := campo1;
    Joins.FieldByName('t1').AsString := tabla1;
    Joins.FieldByName('c2').AsString := campo2;
    Joins.FieldByName('t2').AsString := tabla2;
    Joins.FieldByName('direccion').AsString := joinType;
    Joins.Post;
  end;
end;

procedure TManager.AddField(const ATable, AField, AWhere, AAlias,
  AOrderBy, AOrderPosition: string);
var
  n: integer;
begin
  n := 0;

  if Length(AAlias) = 0 then
  begin
    FSelectedFields.First;
    while not (FSelectedFields.EOF) do
    begin
      if FSelectedFields.FieldByName('campo').AsString = AField then
        Inc(n);
      FSelectedFields.Next;
    end;
  end;

  FSelectedFields.Append;
  FSelectedFields.FieldByName('campo').AsString := AField;
  if n > 0 then
    FSelectedFields.FieldByName('alias').AsString := AField + '_' + IntToStr(n)
  else
    FSelectedFields.FieldByName('alias').AsString := AAlias;
  FSelectedFields.FieldByName('tabla').AsString := ATable;
  FSelectedFields.FieldByName('resultado').AsBoolean := True;
  FSelectedFields.FieldByName('orden').AsString  := AOrderPosition;
  FSelectedFields.FieldByName('tipo').AsString   := AOrderBy;
  FSelectedFields.FieldByName('filtro').AsString := AWhere;
  FSelectedFields.Post;
end;

initialization
  LRUTManager := TManager.Create;

finalization
  LRUTManager.Destroy;
end.

