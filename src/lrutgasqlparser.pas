{
 *****************************************************************************
 *                                                                           *
 *  This file is part of the LazReport Unit Test (LRUT)                      *
 *                                                                           *
 *  See the file gpl.txt, included in this distribution, for details         *
 *  about the copyright.                                                     *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
}

unit LRUTgaSQLParser;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Dialogs, DB, bufdataset, strutils, LCLProc,
  gaAdvancedSQLParser, gaBasicSQLParser, gaSelectStm, gaSQLExpressionParsers,
  gaSQLSelectFieldParsers;

type

  TVariables = record
    Nombre, Valor, NombreParam: string;
  end;

  { TLRUTgaSQLParser }
  TLRUTgaSQLParser = class(TgaAdvancedSQLParser)
  private
    FVariables: TBufDataset;
    FSQL: string;
    function GetVariables: TBufDataset;
    function StartTag(TokenToSearch: string; Position: integer): integer;
    function getTag(sTag: integer; eTag: integer): string;
    function GetSQL: string;
    procedure SetSQL(const AValue: string);
    function ReplaceVariableByParam(AValue: string): string;
    function ReplaceParamByVariable(AValue: string): string;

  protected
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    function ParseSQL(SelectSmt: PChar): boolean;
    function ParseSQL: boolean;
    function BuildSQL: PChar;
    function isValidSQL: boolean;

    procedure GetTables(const TableList: TStrings);
    procedure SetTables(const TableList: TStrings);
    function GetAlias(const Field: string): string;
    procedure GetFields(const FieldList: TStrings);
    procedure SetFields(const FieldList: TStrings);
    function GetOrderBy(const Field: string): string;
    procedure SetOrderBy(const OrderByList: TStrings);
    procedure GetJoins(const memJoins: TBufDataset);
    procedure SetJoins(const memJoins: TBufDataset);
    function GetWhere(const Field: string): string;
    procedure SetWhere(const WhereList: TStrings);

    function ReplaceVariableByValue: string;

    property SQL: string Read GetSQL Write SetSQL;
  end;

implementation

constructor TLRUTgaSQLParser.Create(AOwner: TComponent);
begin
  FVariables := TBufDataset.Create(AOwner);
  FVariables.FieldDefs.Add('nombre', ftString, 250);
  FVariables.FieldDefs.Add('valor', ftString, 250);
  FVariables.FieldDefs.Add('nombreparam', ftString, 250);
//  FVariables.CreateTable;
  FVariables.Active := True;
  inherited Create(AOwner);
end;

function TLRUTgaSQLParser.GetSQL: string;
begin
  GetVariables;

  if FVariables.IsEmpty then
    Result := FSQL
  else
    Result := ReplaceParamByVariable(Self.SQLText.Text);
end;

procedure TLRUTgaSQLParser.SetSQL(const AValue: string);
begin
  FSQL := AValue;
  FVariables.ClearFields;
//  FVariables.Clear(False);

  GetVariables;

  if FVariables.IsEmpty then
    Self.SQLText.Text := AValue
  else
    Self.SQLText.Text := ReplaceVariableByParam(FSQL);
end;

destructor TLRUTgaSQLParser.Destroy;
begin
  FVariables.Active := False;
  FreeAndNil(FVariables);
  inherited Destroy;
end;

function TLRUTgaSQLParser.ParseSQL(SelectSmt: PChar): boolean;
begin
  Self.SQLText.Text := SelectSmt;
  Result := ParseSQL;
end;

function TLRUTgaSQLParser.ParseSQL: boolean;
begin
  Result := False;
  Self.Reset;
  while not (Self.TokenType = stEnd) do
    Self.NextToken;
  Result := True;
end;

function TLRUTgaSQLParser.BuildSQL: PChar;
begin
  Self.Reset;
  Result := PChar(Self.SQLText.Text);
end;

function TLRUTgaSQLParser.isValidSQL: boolean;
begin
  //TODO : Validar SQL :)
  Result := True;
end;

procedure TLRUTgaSQLParser.GetTables(const TableList: TStrings);
begin
  Self.CurrentStatement.AllTables.First;
  repeat
    TableList.Append(Self.CurrentStatement.AllTables.CurrentItem.AsString);
    Self.CurrentStatement.AllTables.Next;
  until Self.CurrentStatement.AllTables.EOF;
end;

procedure TLRUTgaSQLParser.SetTables(const TableList: TStrings);
var
  i: integer;
begin
  Self.SQL := Self.SQL + ' FROM ';
  for i := 0 to TableList.Count - 1 do
  begin
    Self.SQL := Self.SQL + TableList[i];
    if not (i = TableList.Count - 1) then
      Self.SQL := Self.SQL + ', ';
  end;
end;

procedure TLRUTgaSQLParser.GetFields(const FieldList: TStrings);
begin
  Self.CurrentStatement.AllFields.First;
  repeat
    FieldList.Append(Self.CurrentStatement.AllFields.CurrentItem.AsString);
    Self.CurrentStatement.AllFields.Next;
  until Self.CurrentStatement.AllFields.EOF;
end;

procedure TLRUTgaSQLParser.SetFields(const FieldList: TStrings);
var
  i: integer;
begin
  Self.SQL := 'SELECT ';
  for i := 0 to FieldList.Count - 1 do
  begin
    Self.SQL := Self.SQL + FieldList[i];
    if not (i = FieldList.Count - 1) then
      Self.SQL := Self.SQL + ', ';
  end;
end;

procedure TLRUTgaSQLParser.GetJoins(const memJoins: TBufDataset);
var
  jc:    TgaJoinClause;
  jointype: integer;
  delim: TSysCharSet;
begin
  if not (Assigned(TgaSelectSQLStatement(Self.CurrentStatement).JoinClauses)) then
    exit;

  jc := TgaJoinClause.Create(Self.CurrentStatement);
  jc.AsString := TgaSelectSQLStatement(Self.CurrentStatement).JoinClauses.AsString;

  case jc.JoinType of
    jtUnknown: jointype   := 0;
    jtInnerJoin: jointype := 0;
    jtLeftOuterJoin: jointype := 1;
    jtRightOuterJoin: jointype := 2;
    jtFullOuterJoin: jointype := 0;
  end;
  delim := [' ', '.', '='];

  with memJoins do
  begin
    ClearFields;
    // Clear(False);
    Append;
    FieldByName('t1').AsString := ExtractWord(1, jc.JoinOnPredicate.AsString, delim);
    FieldByName('c1').AsString := ExtractWord(2, jc.JoinOnPredicate.AsString, delim);
    FieldByName('t2').AsString := ExtractWord(3, jc.JoinOnPredicate.AsString, delim);
    FieldByName('c2').AsString := ExtractWord(4, jc.JoinOnPredicate.AsString, delim);
    FieldByName('direccion').AsInteger := jointype;
    Post;
  end;

  FreeAndNil(jc);
end;

procedure TLRUTgaSQLParser.SetJoins(const memJoins: TBufDataset);
begin
  //TODO : SetJoins
end;

function TLRUTgaSQLParser.GetAlias(const Field: string): string;
begin
  Self.CurrentStatement.AllFields.First;
  repeat
    if Self.CurrentStatement.AllFields.CurrentItem.AsString = Field then
    begin
      Result := TgaSQLSelectField(
        Self.CurrentStatement.AllFields.CurrentItem).FieldAlias;
      exit;
    end;
    Self.CurrentStatement.AllFields.Next;
  until Self.CurrentStatement.AllFields.EOF;
end;

function TLRUTgaSQLParser.GetOrderBy(const Field: string): string;
begin
  //TODO : GetOrderBy
end;

procedure TLRUTgaSQLParser.SetOrderBy(const OrderByList: TStrings);
var
  tmp: string;
  i:   integer;
begin
  tmp := '';
  for i := 0 to OrderByList.Count - 1 do
  begin
    tmp := tmp + OrderByList[i];
    if not (i = OrderByList.Count - 1) then
      tmp := tmp + ', ';
  end;
  TgaSelectSQLStatement(Self.CurrentStatement).OrderByClause.AsString := tmp;
end;

function TLRUTgaSQLParser.GetWhere(const Field: string): string;
begin
  if not (Assigned(TgaSelectSQLStatement(Self.CurrentStatement).WhereClause)) or
    (Length(TgaSelectSQLStatement(Self.CurrentStatement).WhereClause.AsString) = 0) then
    exit;
  Result := '';

  TgaSQLWhereExpression(TgaSelectSQLStatement(Self.CurrentStatement).WhereClause).First;
  repeat
    if TgaSQLWhereExpression(TgaSelectSQLStatement(
      Self.CurrentStatement).WhereClause).FirstSubPart.AsString = Field then
    begin
      Result := TgaSQLWhereExpression(TgaSelectSQLStatement(
        Self.CurrentStatement).WhereClause).FirstSubPart.NextExpressionPart.AsString +
        TgaSQLWhereExpression(TgaSelectSQLStatement(
        Self.CurrentStatement).WhereClause).LastSubPart.AsString;
      exit;
    end;
    TgaSQLWhereExpression(TgaSelectSQLStatement(Self.CurrentStatement).WhereClause).Next;
  until TgaSQLWhereExpression(TgaSelectSQLStatement(
      Self.CurrentStatement).WhereClause).EOF;
end;

procedure TLRUTgaSQLParser.SetWhere(const WhereList: TStrings);
var
  tmp: string;
  i:   integer;
begin
  tmp := 'WHERE ';
  for i := 0 to WhereList.Count - 1 do
  begin
    tmp := tmp + WhereList[i];
    if not (i = WhereList.Count - 1) then
      tmp := tmp + ', ';
  end;
  TgaSelectSQLStatement(Self.CurrentStatement).WhereClause.AsString := tmp;
end;

function TLRUTgaSQLParser.StartTag(TokenToSearch: string; Position: integer): integer;
var
  Ptr, n: integer;
begin
  Result := 0;

  Ptr := Position;
  n   := Length(TokenToSearch);

  while (Ptr <= Length(FSQL)) do
  begin
    if strlicomp(@FSQL[ptr], PChar(TokenToSearch), n) = 0 then
    begin
      Result := ptr;
      exit;
    end;
    Inc(Ptr);
  end;
end;

function TLRUTgaSQLParser.getTag(sTag: integer; eTag: integer): string;
begin
  if eTag < sTag then
    eTag := Length(FSQL);

  Result := copy(FSQL, sTag, eTag - sTag + 2);
end;

function TLRUTgaSQLParser.GetVariables: TBufDataset;
var
  p0, p1: integer;
begin
  p0 := StartTag('[', 1);
  if P0 = 0 then
    exit; // si el primer tag no se encontro, no tiene caso buscar el segundo
  p1 := StartTag(']', p0);

  while (p0 <> 0) and (p1 <> 0) do
  begin
    FVariables.Append;
    FVariables.FieldByName('nombre').AsString      := getTag(p0, p1 - 1);
    FVariables.FieldByName('nombreparam').AsString := ':' + IntToStr(random(100000));
    FVariables.Post;

    p0 := StartTag('[', p1 + 1);
    if P0 = 0 then
      break;
    p1 := StartTag(']', p0);
  end;
end;

function TLRUTgaSQLParser.ReplaceVariableByValue: string;
begin
  if not (FVariables.IsEmpty) then
  begin
    FVariables.First;
    while not (FVariables.EOF) do
    begin
      FVariables.Edit;
      FVariables.FieldByName('valor').AsString :=
        InputBox('Introduce un valor para la variable',
        FVariables.FieldByName('nombre').AsString, '');
      FVariables.Post;
      FSQL := StringReplace(FSQL, FVariables.FieldByName('nombre').AsString,
        FVariables.FieldByName('valor').AsString, [rfReplaceAll]);
      FVariables.Next;
    end;
  end;
  Result := FSQL;
end;

function TLRUTgaSQLParser.ReplaceVariableByParam(AValue: string): string;
begin
  Result := AValue;
  if not (FVariables.IsEmpty) then
  begin
    FVariables.First;
    while not (FVariables.EOF) do
    begin
      Result := StringReplace(Result, FVariables.FieldByName('nombre').AsString,
        FVariables.FieldByName('nombreparam').AsString, [rfReplaceAll]);
      FVariables.Next;
    end;
  end;
end;

function TLRUTgaSQLParser.ReplaceParamByVariable(AValue: string): string;
begin
  Result := AValue;
  if not (FVariables.IsEmpty) then
  begin
    FVariables.First;
    while not (FVariables.EOF) do
    begin
      Result := StringReplace(Result, FVariables.FieldByName(
        'nombreparam').AsString, FVariables.FieldByName('nombre').AsString,
        [rfReplaceAll]);
      FVariables.Next;
    end;
  end;
end;

end.

