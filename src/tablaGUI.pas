{
 *****************************************************************************
 *                                                                           *
 *  tablaGUI.pas, This file is part of the LazReport Unit Test (LRUT)        *
 *                                                                           *
 *  See the file gpl.txt, included in this distribution, for details         *
 *  about the copyright.                                                     *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
}

unit tablaGUI;

{$mode objfpc}{$H+}

interface

uses
  Classes, Graphics, Grids, Controls, Menus, SysUtils, LCLProc, LCLIntf, sqldb;

type
  TNewFieldEvent = procedure(Sender: TObject; const ATable, AField: string) of object;

  { TTabla }
  TTabla = class(TCustomStringGrid)
  protected
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState;
      X, Y: integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState;
      X, Y: integer); override;
    procedure Close(Sender: TObject);
    procedure Select(Sender: TObject);
    procedure DblClick(Sender: TObject);
    procedure DrawCell(aCol, aRow: integer; aRect: TRect; aState: TGridDrawState);
      override;
    procedure MoveSelection; override;
    procedure DoNewField(const AField: string); virtual;
  private
    difX, difY:  integer;
    Clicked, ClickedToJoin, Titulo: boolean;
    FOnNewField: TNewFieldEvent;
    FField:      string;
    FTable:      string;
  public
    constructor Create(AOwner: TComponent); override;

    procedure SetupLink(ADragOver: TDragOverEvent; ADragDrop: TDragDropEvent);
    procedure Add(AFieldName: string);
    function FieldAt(const X, Y: integer): string;

    property Table: string Read FTable Write FTable;
    property Field: string Read FField Write FField;
    property OnNewField: TNewFieldEvent Read FOnNewField Write FOnNewField;
  end;

implementation

resourcestring
  rsCerrar = 'Cerrar';


{ TTabla }
constructor TTabla.Create(AOwner: TComponent);
var
  c: TGridColumn;
begin
  inherited Create(AOwner);
  Self.OnDblClick := @DblClick;
  Options := Options - [goHorzLine, goVertLine];
  FixedCols := 0;
  ColCount := 2;
  RowCount := 1;
  Self.Clicked := False;
  Self.Titulo := False;
  DragMode := dmAutomatic;
  C := Columns.Add;
  C.ButtonStyle := cbsCheckboxColumn;
  C.SizePriority := 0;
  C.Width := 20;
  Columns.Add;
end;

procedure TTabla.SetupLink(ADragOver: TDragOverEvent; ADragDrop: TDragDropEvent);
begin
  AutoFillColumns := True;
  OnDragOver      := ADragOver;
  OnDragDrop      := ADragDrop;
  if RowCount > 8 then
    Height := DefaultRowHeight * 7 + 5
  else
    Height := DefaultRowHeight * RowCount + 5;
end;

procedure TTabla.Add(AFieldName: string);
var
  ARow: integer;
begin
  ARow     := RowCount;
  RowCount := RowCount + 1;
  Cells[1, ARow] := AFieldName;
end;

function TTabla.FieldAt(const X, Y: integer): string;
var
  ACol, ARow: integer;
begin
  Self.MouseToCell(X, Y, ACol, ARow);
  Result := Cells[1, ARow];
end;

procedure TTabla.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: integer);
var
  popup: TPopupMenu;
  it:    TMenuItem;
  mousepos: TPoint;
begin
  inherited MouseDown(Button, Shift, X, Y);
  // Boton izquierdo
  if Button = mbLeft then
  begin
    self.difX := X;
    self.difY := Y;
    if Y <= Self.DefaultRowHeight then
    begin
      Clicked := True;
      Self.BringToFront;
    end
    else
      ClickedToJoin := True;
  end;
  // Boton derecho
  if Button = mbRight then
  begin
    popup := TpopupMenu.Create(self);

    it := TMenuItem.Create(self);
    it.Caption := Table;
    popup.Items.Add(it);
    it := TMenuItem.Create(self);
    it.Caption := '-';

    it := TMenuItem.Create(self);
    it.Caption := 'SELECT * FROM ' + Table;
    it.OnClick := @Self.Select;
    popup.Items.Add(it);

    it := TMenuItem.Create(self);
    it.Caption := rsCerrar;
    it.OnClick := @Self.Close;
    popup.Items.Add(it);

    GetCursorPos(mousepos);
    popup.PopUp(mousepos.x, mousepos.y);
  end;
end;

procedure TTabla.MouseMove(Shift: TShiftState; X, Y: integer);
begin
  inherited MouseMove(Shift, X, Y);

  if self.Clicked then
  begin
    if self.Top > (0 - difY) then
      self.Top     := self.Top + Y - difY
    else
      self.Clicked := False;

    if self.Left > (0 - difX) then
      self.Left    := self.Left + X - difX
    else
      self.Clicked := False;
  end;
end;

procedure TTabla.Close(Sender: TObject);
begin
  Self.Free;
end;

procedure TTabla.Select(Sender: TObject);
begin
  doNewField('*');
end;

procedure TTabla.DblClick(Sender: TObject);
begin
  DoNewField(FField);
end;

procedure TTabla.DrawCell(aCol, aRow: integer; aRect: TRect; aState: TGridDrawState);
var
  R: TRect;
begin
  if (gdFixed in aState) then
  begin
    ARect := CellRect(0, 0);
    R     := CellRect(ColCount - 1, 0);
    ARect.Right := R.Right;

    Canvas.Pen.Color   := $00ADADAD;
    Canvas.Brush.Color := clWhite;
    Canvas.Rectangle(ARect);

    Inc(ARect.Left, 2);
    Dec(ARect.Right, 2);
    Inc(ARect.Top, 2);
    Dec(ARect.Bottom, 2);

    Canvas.GradientFill(ARect, $00E8E8E8, clWhite, gdVertical);

    Canvas.Brush.Color := clBlack;
    Canvas.Ellipse(ARect.Left + 4, ARect.Top + 4, ARect.Left + 12, ARect.Top + 12);
    Canvas.TextRect(ARect, ARect.Left + 14, ARect.Top + 2, Table);
  end
  else
    inherited DrawCell(ACol, ARow, ARect, AState);
end;

procedure TTabla.MoveSelection;
begin
  FField := Cells[1, Row];
  inherited MoveSelection;
end;

procedure TTabla.DoNewField(const AField: string);
begin
  if assigned(OnNewField) then
    OnNewField(self, Table, AField);
end;

procedure TTabla.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: integer);
begin
  inherited MouseUp(Button, Shift, X, Y);
  Clicked := False;
  ClickedToJoin := False;
end;

end.

