{
 *****************************************************************************
 *                                                                           *
 *  dummy_conf.pas, This file is part of the LazReport Unit Test (LRUT)      *
 *                                                                           *
 *  See the file gpl.txt, included in this distribution, for details         *
 *  about the copyright.                                                     *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
}

unit dummy_conf;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, frm_conf, manager;

var
  FFConf: TLRUTConf;

implementation

initialization
  FFConf := TLRUTConf.Create(nil);
  FFConf.DBGConnectionConf.DataSource := LRUTManager.ConexionDS;
  FFConf.DBGSQLConf.DataSource := LRUTManager.SqlDS;
  FFConf.DBGReportConf.DataSource := LRUTManager.InformeDS;

finalization
  FFConf.Free;

end.

