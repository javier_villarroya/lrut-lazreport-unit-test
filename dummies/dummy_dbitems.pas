{
 *****************************************************************************
 *                                                                           *
 *  dummy_dbitems.pas, This file is part of the LazReport Unit Test (LRUT)   *
 *                                                                           *
 *  See the file gpl.txt, included in this distribution, for details         *
 *  about the copyright.                                                     *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
}

unit dummy_dbitems;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, frm_dbitems;

var
  FFDBItems: TLRUTDBItems;

implementation

initialization
  FFDBItems := TLRUTDBItems.Create(nil);

finalization
  FFDBItems.Free;

end.

