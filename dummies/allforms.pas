unit allforms;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  XMLPropStorage;

type

  { TAllForms }

  TAllForms = class(TForm)
    PropStorage: TXMLPropStorage;
  private
    { private declarations }
  public
    { public declarations }
  end;

implementation

initialization
  {$I allforms.lrs}

end.

