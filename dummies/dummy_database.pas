{
 *****************************************************************************
 *                                                                           *
 *  dummy_database.pas, This file is part of the LazReport Unit Test (LRUT)  *
 *                                                                           *
 *  See the file gpl.txt, included in this distribution, for details         *
 *  about the copyright.                                                     *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
}

unit dummy_database;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, frm_database;

var
  FFDataBase: TLRUTDatabase;

implementation

initialization
  FFDataBase := TLRUTDatabase.Create(nil);

finalization
  FFDataBase.Free;

end.

