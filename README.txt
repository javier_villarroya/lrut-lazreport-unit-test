http://lrut.alasombra.net

LRUT stands for LazReport Unit Test, it's a project trying to be an helpful tool 
to quickly design SQL query based reports.

Packages needed:
SQLDBLaz, lazdbexport, lazreport, lazreportpdfexport : Lazarus included
LazgaSQLParser : svn://alasombra.net/lazgasqlparser
PowerPDF : https://lazarus-ccr.svn.sourceforge.net/svnroot/lazarus-ccr/components/powerpdf
