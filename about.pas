{
 *****************************************************************************
 *                                                                           *
 *  about.pas, This file is part of the LazReport Unit Test (LRUT)           *
 *                                                                           *
 *  See the file gpl.txt, included in this distribution, for details         *
 *  about the copyright.                                                     *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
}

unit about;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  StdCtrls, HelpIntfs, ExtCtrls;

type

  { TAboutFrm }

  TAboutFrm = class(TForm)
    ILicence: TImage;
    Label1:   TLabel;
    Label2:   TLabel;
    LYear:    TLabel;
    LURL:     TLabel;
    MContributors: TMemo;
    procedure LURLClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  AboutFrm: TAboutFrm;

implementation

{ TAboutFrm }

procedure TAboutFrm.LURLClick(Sender: TObject);
var
  err: string;
begin
  if HelpIntfs.ShowHelp(TLabel(Sender).Caption, 'LRUT', 'text/html', err) <>
    shrSuccess then
    ShowMessage(err);
end;

initialization
  {$I about.lrs}

end.

