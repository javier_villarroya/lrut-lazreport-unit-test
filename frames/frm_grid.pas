{
 *****************************************************************************
 *                                                                           *
 *  frm_grid.pas, This file is part of the LazReport Unit Test (LRUT)        *
 *                                                                           *
 *  See the file gpl.txt, included in this distribution, for details         *
 *  about the copyright.                                                     *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
}

unit frm_grid;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, StdCtrls, Buttons, DBGrids,
  Controls;

type

  { TLRUTGrid }

  TLRUTGrid = class(TFrame)
    BEditReport:    TBitBtn;
    BNewReport:     TBitBtn;
    BPDFExporter:   TBitBtn;
    BPrintGrid:     TBitBtn;
    BPrintReport:   TBitBtn;
    BReportPreview: TBitBtn;
    DBGReport:      TDBGrid;
    LReportDescription: TLabel;
    LReportDescriptionData: TLabel;
  private
    { private declarations }
  public
    { public declarations }
  end;

implementation

initialization

{$I frm_grid.lrs}

end.

