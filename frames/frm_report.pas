{
 *****************************************************************************
 *                                                                           *
 *  frm_report.pas, This file is part of the LazReport Unit Test (LRUT)      *
 *                                                                           *
 *  See the file gpl.txt, included in this distribution, for details         *
 *  about the copyright.                                                     *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
}

unit frm_report;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, StdCtrls, Buttons, ComCtrls,
  Dialogs, Controls, lr_e_pdf, gettext, Menus, DB;

type

  { TLRUTReport }

  TLRUTReport = class(TFrame)
    BEditReport:    TBitBtn;
    BNewReport:     TBitBtn;
    BPDFExporter:   TBitBtn;
    BPrintGrid:     TBitBtn;
    BPrintReport:   TBitBtn;
    BReportPreview: TBitBtn;
    LReportDescription: TLabel;
    LReportDescriptionData: TLabel;
    ODOpenReport:   TOpenDialog;
    SDSaveReport:   TSaveDialog;
    procedure BEditReportClick(Sender: TObject);
    procedure BNewReportClick(Sender: TObject);
    procedure BPDFExporterClick(Sender: TObject);
    procedure BPrintGridClick(Sender: TObject);
    procedure BPrintReportClick(Sender: TObject);
    procedure BReportPreviewClick(Sender: TObject);
  private
    { private declarations }
    procedure LoadConf(Sender: TObject);
    procedure LoadConf(Sender: TObject; AReport: string);

  public
    { public declarations }
    SBStatus: TStatusBar;
    Menu:     TMenuItem;
    procedure Translate(ALang: string);
    procedure LoadMenu(AMenu: TMenuItem);
    procedure LoadMenu;
  end;

implementation

uses
  datamodule, manager, dummy_sql;

resourcestring
  rsNew     = 'Nuevo';
  rsVistaPrevia = 'Vista Preliminar';
  rsPrint   = 'Imprimir';
  rsImprimirGrid = 'Imprimir Grid';
  rsMuyBien = '¡Muy bien!';
  rsInforGenerado = 'Se ha generado el nuevo informe %s';
  rsNoInfoInfor = 'No existe informacion del Informe%s¿vamos a insertarlo?';
  rsDescripcionInforme = 'Introduce una descripcion para el informe';
  rsEdit    = 'Editar';
  rsExportarAPDF = 'Exportar a PDF';

{ TLRUTReport }

procedure TLRUTReport.BNewReportClick(Sender: TObject);
begin
  SDSaveReport.InitialDir := LRUTDataModule.acd + 'informes';
  SDSaveReport.Execute;

  LRUTDataModule.Informe.FileName := SDSaveReport.FileName;
  LRUTDataModule.Informe.LoadFromFile(SDSaveReport.FileName);
  LRUTDataModule.Informe.DesignReport;
  SBStatus.SimpleText := Format(rsInforGenerado, [SDSaveReport.Filename]);

  // Comprobamos si ya tenemos guardada la configuracion del informe
  if not (LRUTManager.ReportExists(LRUTDataModule.inf)) then
  begin
    if MessageDlg(rsMuyBien, Format(rsNoInfoInfor, [#10#13]),
      mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    begin
      LRUTDataModule.inf.descripcion :=
        InputBox('Info', rsDescripcionInforme, SDSaveReport.FileName);
      LRUTDataModule.inf.nsql    := LRUTDataModule.sql.archivo;
      LRUTDataModule.inf.archivo := SDSaveReport.FileName;
      LRUTManager.AddReport(LRUTDataModule.inf);
      LoadMenu(Menu);
    end;
  end;
end;

procedure TLRUTReport.BPDFExporterClick(Sender: TObject);
begin
  if LRUTDataModule.Informe.PrepareReport then
    LRUTDataModule.Informe.exportTo(TFrTNPDFExportFilter,
      LRUTDataModule.inf.archivo + '.pdf');
end;

procedure TLRUTReport.BPrintGridClick(Sender: TObject);
begin
  LRUTDataModule.rptPrintGrid.PreviewReport;
end;

procedure TLRUTReport.BPrintReportClick(Sender: TObject);
begin
  if LRUTDataModule.Informe.PrepareReport then
    LRUTDataModule.Informe.PrintPreparedReport('', 1);
end;

procedure TLRUTReport.BReportPreviewClick(Sender: TObject);
begin
  LRUTDataModule.Informe.ShowReport;
end;

procedure TLRUTReport.BEditReportClick(Sender: TObject);
begin
  if FileExists(LRUTDataModule.Informe.FileName) then
    LRUTDataModule.Informe.DesignReport;
end;

procedure TLRUTReport.Translate(ALang: string);
var
  FallbackLang: string;
begin
  FallbackLang := ALang;

  if Alang = '' then
    GetLanguageIDs(ALang, FallbackLang);

  TranslateUnitResourceStrings('formprincipal', 'po' + DirectorySeparator +
    'lrut.' + ALang + '.po');

  BNewReport.Caption     := rsNew;
  BEditReport.Caption    := rsEdit;
  BReportPreview.Caption := rsVistaPrevia;
  BPrintReport.Caption   := rsPrint;
  BPrintGrid.Caption     := rsImprimirGrid;
  BPDFExporter.Caption   := rsExportarAPDF;
end;

procedure TLRUTReport.LoadMenu;
begin
  if Menu = nil then
    exit;
  LoadMenu(Menu);
end;

procedure TLRUTReport.LoadMenu(AMenu: TMenuItem);
var
  registro: TMenuItem;
  qTmp:     TDataSet;
begin
  Menu := AMenu;
  Menu.Clear;
  qTmp := LRUTManager.Reports;
  qTmp.First;
  while not (qTmp.EOF) do
  begin
    registro := TMenuItem.Create(Menu);
    registro.Caption := qTmp.FieldByName('Nombre').AsString;
    registro.OnClick := @LoadConf;
    Menu.Add(registro);
    qTmp.Next;
  end;
end;

procedure TLRUTReport.LoadConf(Sender: TObject);
begin
  LoadConf(Sender, tmenuitem(Sender).Caption);
end;

procedure TLRUTReport.LoadConf(Sender: TObject; AReport: string);
begin
  if LRUTManager.LoadReport(AReport, LRUTDataModule.Inf) then
  begin
    LReportDescriptionData.Caption := LRUTDataModule.inf.descripcion;
    FFSQL.LoadConf(Sender, LRUTDataModule.inf.nsql);

    LRUTDataModule.informe.FileName := LRUTDataModule.inf.archivo;
    LRUTDataModule.informe.LoadFromFile(LRUTDataModule.inf.archivo);
  end;
end;


initialization

{$I frm_report.lrs}

end.

