{
 *****************************************************************************
 *                                                                           *
 *  frm_workzone.pas, This file is part of the LazReport Unit Test (LRUT)    *
 *                                                                           *
 *  See the file gpl.txt, included in this distribution, for details         *
 *  about the copyright.                                                     *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
}

unit frm_workzone;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, ExtCtrls, StdCtrls, DBGrids,
  Buttons, Controls, Dialogs, ComCtrls, gettext;

type

  { TLRUTWorkZone }

  TLRUTWorkZone = class(TFrame)
    BAddJoin:   TBitBtn;
    BDeleteJoin: TBitBtn;
    BGUIToEditor: TBitBtn;
    btnRelaciones: TBitBtn;
    CBFieldA:   TComboBox;
    CBFieldB:   TComboBox;
    CBTableA:   TComboBox;
    CBTableB:   TComboBox;
    DBGRelations: TDBGrid;
    Label1:     TLabel;
    Label2:     TLabel;
    PButtons:   TPanel;
    PJoins:     TPanel;
    RBJoinType: TRadioGroup;
    SBWorkZone: TScrollBox;
    procedure BAddJoinClick(Sender: TObject);
    procedure BDeleteJoinClick(Sender: TObject);
    procedure BGUIToEditorClick(Sender: TObject);
    procedure btnRelacionesClick(Sender: TObject);
    procedure CBTableAChange(Sender: TObject);
    procedure CBTableBChange(Sender: TObject);
    procedure FrameDragDrop(Sender, Source: TObject; X, Y: integer);
    procedure SBWorkZoneDragDrop(Sender, Source: TObject; X, Y: integer);
    procedure SBWorkZoneDragOver(Sender, Source: TObject; X, Y: integer;
      State: TDragState; var Accept: boolean);
  private
    { private declarations }
    procedure TablaDragDrop(Sender, Source: TObject; X, Y: integer);
    procedure TablaDragOver(Sender, Source: TObject; X, Y: integer;
      State: TDragState; var Accept: boolean);
    procedure OnNewField(Sender: TObject; const ATable, AField: string);
    function WorkZoneToSQL: boolean;
  public
    { public declarations }
    procedure Translate(ALang: string);
    procedure CleanWorkZone;
    procedure SQLToWorkZone;
    procedure CreateTableGUI(table: string; const tLeft, tTop: integer);
    function ExistsTableGUI(var table: string): boolean;
  end;

implementation

uses
  main, manager, datamodule, tablaGUI, dummy_sql, dummy_dbitems, dummy_fieldlist;

resourcestring
  rsSQLNoValido  = 'SQL no valido';
  rsBGUIToEditor = 'Pasar Diseño a Editor';

{ TLRUTWorkZone }

procedure TLRUTWorkZone.BAddJoinClick(Sender: TObject);
begin
  if LRUTManager.AddJoin(CBFieldA.Caption, CBTableA.Caption,
    CBFieldB.Caption, CBTableB.Caption, IntToStr(RBJoinType.ItemIndex)) then
  begin
    //JoinToGUI(CBTableA.Caption, CBTableB.Caption, CBFieldA.ItemIndex + 2 , CBTableB.ItemIndex + 2 );
  end;
end;

procedure TLRUTWorkZone.BDeleteJoinClick(Sender: TObject);
begin
  LRUTManager.Joins.Delete;
end;

procedure TLRUTWorkZone.BGUIToEditorClick(Sender: TObject);
begin
  if WorkZoneToSQL then
    if not (MForm.mMultiWindow.Checked) then
      MForm.PCMain.ActivePage := MForm.TSMain;
end;

procedure TLRUTWorkZone.btnRelacionesClick(Sender: TObject);
var
  i: integer;
begin
  if PJoins.Visible then
    PJoins.Visible := False
  else
  begin
    PJoins.Visible := True;
    PJoins.BringToFront;
  end;

  CBTableA.Clear;
  CBTableB.Clear;

  for i := SBWorkZone.ComponentCount - 1 downto 0 do
    CBTableA.AddItem(TTabla(SBWorkZone.Components[i]).Table, SBWorkZone.Components[i]);

  CBTableB.Items := CBTableA.Items;
end;

procedure TLRUTWorkZone.CBTableAChange(Sender: TObject);
begin
  LRUTDataModule.con.SQLConnection.GetFieldNames(CBTableA.Caption, CBFieldA.Items);
  CBTableB.Items.Delete(CBTableB.Items.IndexOf(CBTableA.Caption));
end;

procedure TLRUTWorkZone.CBTableBChange(Sender: TObject);
begin
  LRUTDataModule.con.SQLConnection.GetFieldNames(CBTableB.Caption, CBFieldB.Items);
  CBTableA.Items.Delete(CBTableA.Items.IndexOf(CBTableB.Caption));
end;

function TLRUTWorkZone.ExistsTableGUI(var table: string): boolean;
var
  i: integer;
begin
  Result := False;
  for i := 0 to SBWorkzone.ComponentCount - 1 do
  begin
    if (SBWorkZone.Components[i] is TTabla) and
      (TTabla(SBWorkZone.Components[i]).Table = Table) then
    begin
      Result := True;
      exit;
    end;
  end;
end;

procedure TLRUTWorkZone.FrameDragDrop(Sender, Source: TObject; X, Y: integer);
begin

end;

procedure TLRUTWorkZone.SBWorkZoneDragDrop(Sender, Source: TObject; X, Y: integer);
begin
  if FFDBItems.TVDBItems.Selected.Level = 0 then
    CreateTableGui(FFDBItems.TVDBItems.Selected.Text, X, Y);
end;

procedure TLRUTWorkZone.SBWorkZoneDragOver(Sender, Source: TObject;
  X, Y: integer; State: TDragState; var Accept: boolean);
begin
  Accept := (Source is TTreeView) and (FFDBItems.TVDBItems.Selected.Level = 0);
end;

procedure TLRUTWorkZone.CreateTableGUI(table: string; const tLeft, tTop: integer);
var
  tmpFields: TStringList;
  i:     integer;
  Tabla: TTabla;
begin
  if ExistsTableGUI(table) then
  begin
    ShowMessage('La tabla ya esta añadida');
    exit;
  end;

  tmpFields := TStringList.Create;
  LRUTDataModule.md.GetFieldsFromTable(table, tmpFields);

  Tabla := TTabla.Create(SBWorkZone);
  Tabla.Table := trim(table);
  for i := 1 to tmpFields.Count do
    Tabla.Add(tmpFields[i - 1]);
  Tabla.SetupLink(@TablaDragOver, @TablaDragDrop);
  Tabla.Left   := tLeft;
  Tabla.Top    := tTop;
  Tabla.Parent := SBWorkZone;
  Tabla.OnNewField := @OnNewField;

  tmpFields.Free;
end;

procedure TLRUTWorkZone.TablaDragDrop(Sender, Source: TObject; X, Y: integer);
var
  ATabla, AField, BTabla, BField: string;
begin
  if Sender = Source then
    exit;

  ATabla := (Source as TTabla).Table;
  AField := (Source as TTabla).Field;
  BTabla := (Sender as TTabla).Table;
  BField := (Sender as TTabla).FieldAt(X, Y);

  LRUTManager.AddJoin(AField, ATabla, BField, BTabla, '0');

  //JoinToGUI(ATabla,BTabla, ARow, BRow);
end;

procedure TLRUTWorkZone.TablaDragOver(Sender, Source: TObject;
  X, Y: integer; State: TDragState; var Accept: boolean);
begin
  Accept := Source is TTabla;
end;

procedure TLRUTWorkZone.OnNewField(Sender: TObject; const ATable, AField: string);
begin
  FFFieldList.AddFieldNode(ATable, AField, '', '', '', '');
end;

procedure TLRUTWorkZone.CleanWorkZone;
var
  i: integer;
begin
  for i := SBWorkZone.ComponentCount - 1 downto 0 do
    SBWorkZone.Components[i].Free;
end;

function TLRUTWorkZone.WorkZoneToSQL: boolean;
var
  tmpList, tmpWhere, tmpOrderBy: TStringList;
  tmpString: string;
  i: integer;
begin
  Result     := False;
  //Campos & Where (Incorrecto) & Alias
  tmpList    := TStringList.Create;
  tmpWhere   := TStringList.Create;
  tmpOrderBy := TStringList.Create;
  LRUTManager.SelectedFields.First;
  while not (LRUTManager.SelectedFields.EOF) do
  begin
    if LRUTManager.SelectedFields.FieldByName('resultado').AsBoolean then
    begin
      tmpString := LRUTManager.SelectedFields.FieldByName('tabla').AsString +
        '.' + LRUTManager.SelectedFields.FieldByName('campo').AsString;
      if Length(LRUTManager.SelectedFields.FieldByName('tipo').AsString) > 0 then
        tmpOrderBy.Append(tmpString + ' ' +
          LRUTManager.SelectedFields.FieldByName('tipo').AsString);
      if Length(LRUTManager.SelectedFields.FieldByName('filtro').AsString) > 0 then
        tmpWhere.Append(tmpString + LRUTManager.SelectedFields.FieldByName(
          'filtro').AsString);
      if Length(LRUTManager.SelectedFields.FieldByName('alias').AsString) > 0 then
        tmpString := tmpString + ' AS ' +
          LRUTManager.SelectedFields.FieldByName('alias').AsString;
      tmpList.Append(tmpString);
    end;
    LRUTManager.SelectedFields.Next;
  end;
  LRUTDataModule.sql.SQLParser.SetFields(tmpList);
  if tmpWhere.Count > 0 then
    LRUTDataModule.sql.SQLParser.SetWhere(tmpWhere);

  //Tablas
  tmpList.Clear;
  for i := SBWorkZone.ComponentCount - 1 downto 0 do
    tmpList.Append(TTabla(SBWorkZone.Components[i]).Table);
  LRUTDataModule.sql.SQLParser.SetTables(tmpList);

  //Joins
  if not (LRUTManager.Joins.IsEmpty) then
    LRUTDataModule.sql.SQLParser.SetJoins(LRUTManager.Joins);

  //Order by
  if tmpOrderBy.Count > 0 then
    LRUTDataModule.sql.SQLParser.SetOrderBy(tmpOrderBy);

  //TODO : Having

  FreeAndNil(tmpList);
  FreeAndNil(tmpWhere);
  FreeAndNil(tmpOrderBy);

  LRUTDataModule.sql.SQLParser.ParseSQL;
  if not (LRUTDataModule.sql.SQLParser.isValidSQL) then
  begin
    ShowMessage(rsSQLNoValido);
    exit;
  end;

  FFSQL.SMSQL.ClearAll;
  FFSQL.SMSQL.Text := LRUTDataModule.sql.SQLParser.SQL;
  Result := True;
end;

procedure TLRUTWorkZone.Translate(ALang: string);
var
  FallbackLang: string;
begin
  FallbackLang := ALang;

  if Alang = '' then
    GetLanguageIDs(ALang, FallbackLang);

  TranslateUnitResourceStrings('frm_workzone', 'po' + DirectorySeparator +
    'lrut.' + ALang + '.po');
  BGUIToEditor.Caption := rsBGUIToEditor;
end;

procedure TLRUTWorkZone.SQLToWorkZone;
var
  i, j: integer;
  TableList, FieldList: TStringList;
  Table, FieldAlias, Field, Orderby, Where: string;
begin
  if SBWorkZone.ComponentCount > 0 then
    CleanWorkZone;

  if not (LRUTManager.SelectedFields.IsEmpty) then
    LRUTManager.SelectedFields.ClearFields;

  LRUTDataModule.sql.SQLParser.CurrentStatement.Clear;
  LRUTDataModule.sql.SQLParser.SQL := FFSQL.SMSQL.Lines.Text;
  LRUTDataModule.sql.SQLParser.ParseSQL;

  if LRUTDataModule.sql.SQLParser.isValidSQL then
  begin
    TableList := TStringList.Create;
    LRUTDataModule.sql.SQLParser.GetTables(TableList);
    for i := 0 to TableList.Count - 1 do
      CreateTableGui(TableList[i], (10 * i) + 10, 10);

    FieldList := TStringList.Create;
    LRUTDataModule.sql.SQLParser.GetFields(FieldList);
    // TODO : Esta forma de añadir el "where" hace que no se respete las condiciones
    j := 0;
    for i := 0 to FieldList.Count - 1 do
    begin
      Table      := LRUTDataModule.md.SearchUniqueTable(TableList, FieldList[i]);
      FieldAlias := LRUTDataModule.sql.SQLParser.GetAlias(FieldList[i]);
      Field      := FieldList[i];
      Orderby    := LRUTDataModule.sql.SQLParser.GetOrderBy(FieldList[i]);
      if Length(Orderby) > 0 then
        Inc(j);
      Where := LRUTDataModule.sql.SQLParser.GetWhere(FieldList[i]);
      FFFieldList.AddFieldNode(Table, Field, Where, FieldAlias,
        OrderBy, IntToStr(j));
    end;

    TableList.Free;
    FieldList.Free;
    LRUTDataModule.sql.SQLParser.GetJoins(LRUTManager.Joins);
  end;
end;

initialization

{$I frm_workzone.lrs}

end.

