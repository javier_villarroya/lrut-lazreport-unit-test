{
 *****************************************************************************
 *                                                                           *
 *  frm_fieldlist.pas, This file is part of the LazReport Unit Test (LRUT)   *
 *                                                                           *
 *  See the file gpl.txt, included in this distribution, for details         *
 *  about the copyright.                                                     *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
}

unit frm_fieldlist;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, DBGrids, Menus, Controls,
  ComCtrls;

type

  { TLRUTFieldList }

  TLRUTFieldList = class(TFrame)
    MIAdd:    TMenuItem;
    MIAggregate: TMenuItem;
    MIDelete: TMenuItem;
    PMFieldsList: TPopupMenu;
    SGFieldsList: TDBGrid;
    procedure MIAddClick(Sender: TObject);
    procedure MIAggregateClick(Sender: TObject);
    procedure MIDeleteClick(Sender: TObject);
    procedure SGFieldsListDragDrop(Sender, Source: TObject; X, Y: integer);
    procedure SGFieldsListDragOver(Sender, Source: TObject; X, Y: integer;
      State: TDragState; var Accept: boolean);
  private
    { private declarations }
  public
    { public declarations }
    procedure AddFieldNode(const ATable, AField, AWhere, AAlias,
      AOrderBy, AOrderPosition: string);
    procedure Clean;
  end;

implementation

uses
  dummy_dbitems, manager, dummy_workzone, tablaGUI, datamodule;

procedure TLRUTFieldList.SGFieldsListDragOver(Sender, Source: TObject;
  X, Y: integer; State: TDragState; var Accept: boolean);
begin
  Accept := ((Source is TTreeView) and (FFDBItems.TVDBItems.Selected.Level = 1)) or
    Source is TTabla;
end;

procedure TLRUTFieldList.SGFieldsListDragDrop(Sender, Source: TObject; X, Y: integer);
var
  field, table: string;
begin
  if (Source is TTreeView) then
  begin
    table := FFDBItems.TVDBItems.Selected.Parent.Text;
    field := FFDBItems.TVDBItems.Selected.Text;
  end
  else
  begin
    table := TTabla(Source).Table;
    field := TTabla(Source).Field;
  end;

  if not (FFWorkZone.ExistsTableGUI(table)) then
    FFWorkZone.CreateTableGUI(table, 10, 10);

  AddFieldNode(table, field, '', '', '', '');
end;

procedure TLRUTFieldList.MIAddClick(Sender: TObject);
begin
  LRUTManager.SelectedFields.Append;
  LRUTDataModule.md.GetTableNames(SGFieldsList.Columns[2].PickList);
end;

procedure TLRUTFieldList.MIAggregateClick(Sender: TObject);
begin
  if MIAggregate.Checked then
    MIAggregate.Checked := False
  else
    MIAggregate.Checked := True;
  SGFieldsList.Columns[6].Visible := True;
end;

procedure TLRUTFieldList.MIDeleteClick(Sender: TObject);
begin
  LRUTManager.SelectedFields.Delete;
end;

procedure TLRUTFieldList.Clean;
var
  i: integer;
begin
  for i := SGFieldsList.Columns.Count - 1 downto 0 do
    SGFieldsList.Columns.Delete(i);
  SGFieldsList.ReadOnly  := True;
  SGFieldsList.PopupMenu := nil;
end;

procedure TLRUTFieldList.AddFieldNode(const ATable, AField, AWhere,
  AAlias, AOrderBy, AOrderPosition: string);
begin
  LRUTManager.AddField(ATable, AField, AWhere, AAlias, AOrderBy, AOrderPosition);
end;


initialization

{$I frm_fieldlist.lrs}

end.

