{
 *****************************************************************************
 *                                                                           *
 *  frm_database.pas, This file is part of the LazReport Unit Test (LRUT)    *
 *                                                                           *
 *  See the file gpl.txt, included in this distribution, for details         *
 *  about the copyright.                                                     *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
}

unit frm_database;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, StdCtrls, EditBtn, Buttons,
  Dialogs, Controls, ComCtrls, ExtCtrls, gettext, Menus, DB, LCLproc;

type

  { TLRUTDatabase }

  TLRUTDatabase = class(TFrame)
    BConnect: TBitBtn;
    CBDBType: TComboBox;
    CBSavePassword: TCheckBox;
    EDB:      TEditButton;
    EHost:    TEdit;
    EPassword: TEdit;
    EUser:    TEdit;
    LConnectionName: TLabel;
    LConnectionNameData: TLabel;
    LDB:      TLabel;
    LDBType:  TLabel;
    LHost:    TLabel;
    LPassword: TLabel;
    LUser:    TLabel;
    procedure BConnectClick(Sender: TObject);
    procedure CBDBTypeChange(Sender: TObject);
    procedure CBSavePasswordChange(Sender: TObject);
    procedure EDBButtonClick(Sender: TObject);
    procedure EDBChange(Sender: TObject);
    procedure EHostChange(Sender: TObject);
    procedure EPasswordChange(Sender: TObject);
    procedure EUserChange(Sender: TObject);
  private
    { private declarations }
    procedure ConnectedState(AEstate: boolean);
    procedure LoadDatabaseConf(Sender: TObject);
    procedure LoadDatabaseConf(Sender: TObject; ADatabase: string);

  public
    { public declarations }
    SBStatus: TStatusBar;
    Menu:     TMenuItem;
    procedure Translate(ALang: string);
    procedure LoadMenu(AMenu: TMenuItem);
    procedure LoadMenu;
  end;

implementation

uses
  datamodule, manager, main, dummy_sql, dummy_report, dummy_workzone, dummy_fieldlist;

resourcestring
  rsConecta  = 'Conectar';
  rsDesconecta = 'Desconectar';
  rsIntroduceNConexion = 'Introduce un nombre para la configuracion';
  rsDesconectado = 'Desconectado';
  rsUsuario  = 'Usuario :';
  rsPassword = 'Password :';
  rsHost     = 'Host :';
  rsTipoBBDD = 'Tipo BBDD :';
  rsBBDD     = 'BBDD :';
  rsNombre   = 'Nombre :';
  rsGuardaPass = 'Guardar Password';
  rsConectadoA = 'Conectado a %s Correctamente';
  rsMuyBien  = '¡Muy bien!';
  rsConCorrecta = 'La conexion ha sido correcta %s ¿Quieres guardar la configuracion?';
  rsInfo     = 'Info';
  rsNoSeHaPodido = 'No se ha podido conectar a %s';

{ TLRUTDatabase }
procedure TLRUTDatabase.BConnectClick(Sender: TObject);
begin
  if not (LRUTDataModule.con.SQLConnection.Connected) then
  begin
    LRUTDataModule.con.setConnectionDBType(CBDBType.ItemIndex);
    if LRUTDataModule.Con.SQLConnection = nil then
      exit;

    EUser.Text := LRUTDataModule.con.UserName;
    EPassword.Text := LRUTDataModule.con.Password;
    EHost.Text := LRUTDataModule.con.HostName;
    EDB.Text := LRUTDataModule.con.DatabaseName;
    LRUTDataModule.con.SQLConnection.Connected := True;
    DebugLn(LRUTDataModule.con.SQLConnection.ClassName + '?');

    if (LRUTDataModule.con.SQLConnection.Connected) then
    begin
      ConnectedState(True);
      LRUTDataModule.md.Connection := LRUTDataModule.con.SQLConnection;

      SBStatus.SimpleText := Format(rsConectadoA, [EDB.Text]);
      LRUTDataModule.rptQuery.DataBase := LRUTDataModule.con.SQLConnection;
      LRUTDataModule.qTransaction.DataBase := LRUTDataModule.con.SQLConnection;

      if not (LRUTManager.ConnectionExists(LRUTDataModule.con)) then
      begin
        if MessageDlg(rsMuyBien, Format(rsConCorrecta, [#10#13]),
          mtConfirmation, [mbYes, mbNo], 0) = mrYes then
        begin
          LRUTDataModule.con.ConnectionName :=
            InputBox(rsInfo, rsIntroduceNConexion, EDB.Text);
          LConnectionNameData.Caption := LRUTDataModule.con.ConnectionName;
          if not (CBSavePassword.Checked) then
            LRUTDataModule.con.Password := '';
          LRUTManager.AddConnection(LRUTDataModule.con);
          LoadMenu(Menu);
        end;
      end
      else
      if CBSavePassword.Checked then
        LRUTManager.UpdatePasswordConnection(LRUTDataModule.con.ConnectionName,
          LRUTDataModule.con.Password)
      else
        LRUTManager.UpdatePasswordConnection(
          LRUTDataModule.con.ConnectionName, '');
    end
    else
      SBStatus.SimpleText := Format(rsNoSeHaPodido, [EDB.Text]);
  end
  else
    ConnectedState(False);

  if (LRUTDataModule.con.SQLConnection <> nil) and
    LRUTDataModule.con.SQLConnection.Connected then
    MForm.ShowSQLBuilder;
end;

procedure TLRUTDatabase.CBDBTypeChange(Sender: TObject);
begin
  LRUTDataModule.con.DBType := CBDBType.ItemIndex;
end;

procedure TLRUTDatabase.CBSavePasswordChange(Sender: TObject);
begin
  LRUTDataModule.con.savePassword := CBSavePassword.Enabled;
  if CBSavePassword.Checked then
    LRUTManager.UpdatePasswordConnection(LRUTDataModule.con.ConnectionName,
      LRUTDataModule.con.Password)
  else
    LRUTManager.UpdatePasswordConnection(LRUTDataModule.con.ConnectionName, '');
end;

procedure TLRUTDatabase.EDBButtonClick(Sender: TObject);
begin
  LRUTDataModule.ODDatabase.InitialDir := LRUTDataModule.acd;
  if LRUTDataModule.ODDatabase.Execute then
    EDB.Text := LRUTDataModule.ODDatabase.FileName;
end;

procedure TLRUTDatabase.EDBChange(Sender: TObject);
begin
  LRUTDataModule.con.DatabaseName := EDB.Text;
end;

procedure TLRUTDatabase.EHostChange(Sender: TObject);
begin
  LRUTDataModule.con.HostName := EHost.Text;
end;

procedure TLRUTDatabase.EPasswordChange(Sender: TObject);
begin
  LRUTDataModule.con.Password := EPassword.Text;
end;

procedure TLRUTDatabase.EUserChange(Sender: TObject);
begin
  LRUTDataModule.con.UserName := EUser.Text;
end;

procedure TLRUTDatabase.ConnectedState(AEstate: boolean);
begin
  if AEstate then
  begin
    EUser.ReadOnly   := True;
    EPassword.ReadOnly := True;
    EHost.ReadOnly   := True;
    EDB.ReadOnly     := True;
    CBDBType.Enabled := False;
    Menu.Enabled     := False;
    BConnect.Caption := rsDesconecta;
  end
  else
    with LRUTDataModule do
    begin
      rptQuery.Active  := False;
      con.SQLConnection.Connected := False;
      BConnect.Caption := rsConecta;
      SBStatus.SimpleText := rsDesconectado;
      con.ConnectionName := '';
      sql.descripcion  := '';
      inf.descripcion  := '';
      EUser.ReadOnly   := False;
      EPassword.ReadOnly := False;
      CBSavePassword.Checked := False;
      EHost.ReadOnly   := False;
      EDB.ReadOnly     := False;
      CBDBType.Enabled := True;
      FFSQL.BEditorToGUI.Enabled := True;
      Menu.Enabled     := True;
      EUser.Clear;
      EPassword.Clear;
      EHost.Clear;
      EDB.Clear;
      CBDBType.ItemIndex := -1;
      LConnectionNameData.Caption := con.ConnectionName;
      FFSQL.LSQLDescriptionData.Caption := sql.descripcion;
      FFReport.LReportDescriptionData.Caption := inf.descripcion;
      FFSQL.SMSQL.ClearAll;
      BConnect.Caption := rsConecta;

      FFWorkZone.CleanWorkZone;
      FFFieldList.Clean;

      LRUTManager.Joins.ClearFields;
      LRUTManager.SelectedFields.ClearFields;
    end;
end;

procedure TLRUTDatabase.Translate(ALang: string);
var
  FallbackLang: string;
begin
  FallbackLang := ALang;

  if Alang = '' then
    GetLanguageIDs(ALang, FallbackLang);

  TranslateUnitResourceStrings('frm_database', 'po' + DirectorySeparator +
    'lrut.' + ALang + '.po');

  LConnectionName.Caption := rsNombre;
  LUser.Caption    := rsUsuario;
  LPassword.Caption := rsPassword;
  LHost.Caption    := rsHost;
  LDBType.Caption  := rsTipoBBDD;
  LDB.Caption      := rsBBDD;
  CBSavePassword.Caption := rsGuardaPass;
  BConnect.Caption := rsConecta;
end;

procedure TLRUTDatabase.LoadMenu;
begin
  if Menu = nil then
    exit;
  LoadMenu(Menu);
end;

procedure TLRUTDatabase.LoadMenu(AMenu: TMenuItem);
var
  registro: TMenuItem;
  qTmp:     TDataSet;
begin
  Menu := AMenu;
  Menu.Clear;
  qTmp := LRUTManager.Connections;
  qTmp.First;
  while not (qTmp.EOF) do
  begin
    registro := TMenuItem.Create(Menu);
    registro.Caption := qTmp.FieldByName('Nombre').AsString;
    registro.OnClick := @LoadDatabaseConf;
    Menu.Add(registro);
    qTmp.Next;
  end;
end;

procedure TLRUTDatabase.LoadDatabaseConf(Sender: TObject);
begin
  LoadDatabaseConf(Sender, tmenuitem(Sender).Caption);
end;

procedure TLRUTDatabase.LoadDatabaseConf(Sender: TObject; ADatabase: string);
begin
  if LRUTManager.LoadConnection(ADatabase, LRUTDataModule.Con) then
  begin
    LConnectionNameData.Caption := LRUTDataModule.con.ConnectionName;
    EUser.Text := LRUTDataModule.con.UserName;
    EHost.Text := LRUTDataModule.con.HostName;
    CBDBType.ItemIndex := LRUTDataModule.con.DBType;
    EDB.Text   := LRUTDataModule.con.DatabaseName;
    EPassword.Text := LRUTDataModule.con.Password;

    if LRUTDataModule.con.savePassword then
    begin
      CBSavePassword.Checked := True;
      BConnectClick(Sender);
    end;
  end;
end;

initialization

{$I frm_database.lrs}

end.

