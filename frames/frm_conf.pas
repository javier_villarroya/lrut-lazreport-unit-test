{
 *****************************************************************************
 *                                                                           *
 *  frm_conf.pas, This file is part of the LazReport Unit Test (LRUT)        *
 *                                                                           *
 *  See the file gpl.txt, included in this distribution, for details         *
 *  about the copyright.                                                     *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
}

unit frm_conf;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, ComCtrls, StdCtrls, ExtCtrls,
  DBGrids, Controls, Dialogs, LCLType, gettext;

type

  { TLRUTConf }

  TLRUTConf = class(TFrame)
    DBGConnectionConf: TDBGrid;
    DBGReportConf: TDBGrid;
    DBGSQLConf: TDBGrid;
    IConnection: TImage;
    IReport:   TImage;
    ISQL:      TImage;
    LConnections: TLabel;
    LInfo:     TLabel;
    LReports:  TLabel;
    LSQL:      TLabel;
    recursivo: TCheckBox;
    procedure DBGConnectionConfDblClick(Sender: TObject);
    procedure DBGConnectionConfKeyUp(Sender: TObject; var Key: word;
      Shift: TShiftState);
    procedure DBGReportConfDblClick(Sender: TObject);
    procedure DBGReportConfKeyUp(Sender: TObject; var Key: word;
      Shift: TShiftState);
    procedure DBGSQLConfDblClick(Sender: TObject);
    procedure DBGSQLConfKeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
  private
    { private declarations }
    procedure DeleteConnectionConf;
    procedure DeleteReportConf;
    procedure DeleteSQLConf;

  public
    { public declarations }
    procedure Translate(ALang: string);
  end;

implementation

uses
  dummy_database, dummy_sql, dummy_report, manager;

resourcestring
  rsQuieresBorrar = '¿Quieres borrar la conexion %s ?';
  rsBorrarSQL = '¿Quieres borrar la SQL %s ?';
  rsBorrarInforme = '¿Quieres borrar informe %s ?';
  rsLConnections = 'Conexiones';
  rsSQL      = 'SQL';
  rsLReports = 'Informes';
  rsLinfo    = 'Doble click sobre el registro a borrar';
  rsRecursivo = 'Borrado recursivo';



{ TLRUTConf }
procedure TLRUTConf.DeleteConnectionConf;
var
  msg: string;
begin
  msg := Format(rsQuieresBorrar,
    [LRUTManager.Connections.FieldByName('nombre').AsString]);
  if MessageDlg('Question', msg, mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    LRUTManager.DeleteConnection(recursivo.Checked);
  FFDatabase.LoadMenu;
end;

procedure TLRUTConf.DeleteReportConf;
var
  msg: string;
begin
  msg := Format(rsBorrarInforme,
    [LRUTManager.Reports.FieldByName('archivo').AsString]);
  if MessageDlg('Question', msg, mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    LRUTManager.DeleteReport(LRUTManager.Reports.FieldByName('archivo').AsString,
      recursivo.Checked);
  FFReport.LoadMenu;
end;

procedure TLRUTConf.DeleteSQLConf;
var
  msg: string;
begin
  msg := Format(rsBorrarSQL, [LRUTManager.SQLs.FieldByName('archivo').AsString]);
  if MessageDlg('Question', msg, mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    LRUTManager.DeleteSQL(LRUTManager.SQLs.FieldByName('archivo').AsString,
      recursivo.Checked);
  FFSQL.LoadMenu;
end;

procedure TLRUTConf.DBGConnectionConfDblClick(Sender: TObject);
begin
  DeleteConnectionConf;
end;

procedure TLRUTConf.DBGConnectionConfKeyUp(Sender: TObject; var Key: word;
  Shift: TShiftState);
begin
  if key = VK_DELETE then
    DeleteConnectionConf;
end;

procedure TLRUTConf.DBGReportConfDblClick(Sender: TObject);
begin
  DeleteReportConf;
end;

procedure TLRUTConf.DBGReportConfKeyUp(Sender: TObject; var Key: word;
  Shift: TShiftState);
begin
  if key = VK_DELETE then
    DeleteReportConf;
end;

procedure TLRUTConf.DBGSQLConfDblClick(Sender: TObject);
begin
  DeleteSQLConf;
end;

procedure TLRUTConf.DBGSQLConfKeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
begin
  if key = VK_DELETE then
    DeleteSQLConf;
end;

procedure TLRUTConf.Translate(ALang: string);
var
  FallbackLang: string;
begin
  FallbackLang := ALang;

  if Alang = '' then
    GetLanguageIDs(ALang, FallbackLang);

  LConnections.Caption := rsLConnections;
  LSQL.Caption      := rsSQL;
  LReports.Caption  := rsLReports;
  LInfo.Caption     := rsLinfo;
  recursivo.Caption := rsRecursivo;
  DBGConnectionConf.Columns.Items[0].Title.Caption := 'Nombre';
  DBGConnectionConf.Columns.Items[1].Title.Caption := 'Base de Datos';
  DBGConnectionConf.Columns.Items[2].Title.Caption := 'Servidor';
  DBGConnectionConf.Columns.Items[3].Title.Caption := 'Usuario';
  DBGSQLConf.Columns.Items[0].Title.Caption := 'Archivo';
  DBGSQLConf.Columns.Items[1].Title.Caption := 'Descripcion';
  DBGSQLConf.Columns.Items[2].Title.Caption := 'Conexion';
  DBGReportConf.Columns.Items[0].Title.Caption := 'Archivo';
  DBGReportConf.Columns.Items[1].Title.Caption := 'Descripcion';
  DBGReportConf.Columns.Items[2].Title.Caption := 'SQL';

end;

initialization

{$I frm_conf.lrs}

end.

