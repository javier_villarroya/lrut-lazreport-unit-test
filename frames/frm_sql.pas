{
 *****************************************************************************
 *                                                                           *
 *  frm_sql.pas, This file is part of the LazReport Unit Test (LRUT)         *
 *                                                                           *
 *  See the file gpl.txt, included in this distribution, for details         *
 *  about the copyright.                                                     *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
}

unit frm_sql;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, StdCtrls, EditBtn, Buttons,
  Dialogs, Controls, ComCtrls, gettext, SynEdit, LCLType, Menus, DB;

type

  { TLRUTSQL }

  TLRUTSQL = class(TFrame)
    BEditorToGUI: TBitBtn;
    BRunSQL: TBitBtn;
    BSaveSQL: TBitBtn;
    ds:      TEdit;
    LBFound: TListBox;
    LSQLDescription: TLabel;
    LSQLDescriptionData: TLabel;
    SMSQL:   TSynEdit;
    uLabel1: TLabel;
    procedure BEditorToGUIClick(Sender: TObject);
    procedure BRunSQLClick(Sender: TObject);
    procedure BSaveSQLClick(Sender: TObject);
    procedure LBFoundKeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
    procedure SMSQLChange(Sender: TObject);
    function SearchToLBFound(toSearch: string): boolean;
    function DotSearchToLBFound(toSearch: string): boolean;
    procedure SMSQLKeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
  private
    { private declarations }
    procedure LoadConf(Sender: TObject);
  public
    { public declarations }
    SBStatus: TStatusBar;
    Menu:     TMenuItem;
    procedure Translate(ALang: string);
    procedure LoadMenu(AMenu: TMenuItem);
    procedure LoadMenu;
    procedure LoadConf(Sender: TObject; ASQL: string);
    procedure SQLToWorkZone;
  end;

implementation

uses
  datamodule, manager;

const
  autoTokens: array[1..16] of string =
    ('select', 'from', 'where', 'group by', 'having', 'order by', 'join', 'inner join',
    'left outer join', 'left join', 'right outer join', 'right join',
    'full outer join', 'full join', 'union join', 'cross join');

resourcestring
  rsIntroduceNSQL = 'Introduce una descripcion sobre la SQL';
  rsDescripcion = 'Descripcion :';
  rsMuyBien   = '¡Muy bien!';
  rsInfo      = 'Info';
  rsNoInfoSQL = 'No existe informacion del SQL%s¿vamos a insertarlo?';
  rsDebesConectar = 'Deberias conectarte a alguna BBDD primero';
  rsmGuarda   = 'Guardar';
  rsEjecuta   = 'Ejecutar';

{ TLRUTSQL }

procedure TLRUTSQL.BEditorToGUIClick(Sender: TObject);
begin
  if Length(SMSQL.Text) > 0 then
    SQLToWorkZone;
end;

procedure TLRUTSQL.BRunSQLClick(Sender: TObject);
begin
  LRUTDataModule.sql.Query.DataBase := LRUTDataModule.con.SQLConnection;
  LRUTDataModule.sql.Transaction.DataBase := LRUTDataModule.con.SQLConnection;

  // Mientras no se corrija el posible bug de lazreport
  LRUTDataModule.rptQuery.DataBase     := LRUTDataModule.con.SQLConnection;
  LRUTDataModule.qTransaction.DataBase := LRUTDataModule.con.SQLConnection;

  if LRUTDataModule.con.SQLConnection.Connected then
  begin
    // reemplazar variables de usuario en el codigo SQL
    LRUTDataModule.sql.SQLParser.SQL := SMSQL.Lines.Text;
    LRUTDataModule.sql.Query.Active  := False;
    LRUTDataModule.sql.Query.SQL.Clear;
    LRUTDataModule.sql.Query.SQL.Text :=
      LRUTDataModule.sql.SQLParser.ReplaceVariableByValue;
    LRUTDataModule.sql.Query.Active   := True;

    // Lo mismo aqui...
    with LRUTDataModule do
    begin
      rptQuery.Active := False;
      rptQuery.SQL.Clear;
      rptQuery.SQL.Assign(sql.Query.SQL);
      rptQuery.Active := True;
    end;
  end
  else
    SBStatus.SimpleText := rsDebesConectar;

  LRUTDataModule.rptDatasource.DataSet := LRUTDataModule.sql.Query;
  LRUTDataModule.rptDataSet.DataSet    := LRUTDataModule.sql.Query;
end;

procedure TLRUTSQL.BSaveSQLClick(Sender: TObject);
begin
  if (SMSQL.Lines.Count = 0) or not (LRUTDataModule.con.SQLConnection.Connected) then
    exit;
  // Guardamos el contenido del "memo" al archivo .sqlGroup
  LRUTDataModule.SDSaveSQL.InitialDir := LRUTDataModule.acd + 'sql';
  if not LRUTDataModule.SDSaveSQL.Execute then
    exit;

  SMSQL.Lines.SaveToFile(LRUTDataModule.SDSaveSQL.FileName);

  // Comprobamos si ya tenemos guardada la configuracion de la sqlGroup
  if not (LRUTManager.SQLExists(LRUTDataModule.sql)) then
  begin
    if MessageDlg(rsMuyBien, Format(rsNoInfoSQL, [#10#13]), mtConfirmation,
      [mbYes, mbNo], 0) = mrYes then
    begin
      LRUTDataModule.sql.descripcion :=
        InputBox(rsInfo, rsIntroduceNSQL, LRUTDataModule.SDSaveSQL.FileName);
      LRUTDataModule.sql.nconexion   := LRUTDataModule.con.ConnectionName;
      LRUTDataModule.sql.archivo     := LRUTDataModule.SDSaveSQL.FileName;
      LRUTManager.AddSQL(LRUTDataModule.sql);
      LSQLDescriptionData.Caption := LRUTDataModule.sql.descripcion;
      LoadMenu(Menu);
    end;
  end;
end;

procedure TLRUTSQL.LBFoundKeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
begin
  if key = VK_ESCAPE then
    LBFound.Visible := False;

  if key = VK_RETURN then
  begin
    SMSQL.SelText   := RightStr(LBFound.Items[LBFound.ItemIndex],
      Length(LBFound.Items[LBFound.ItemIndex]) -
      Length(SMSQL.GetWordAtRowCol(SMSQL.CaretXY)));
    LBFound.Visible := False;
  end;
  SMSQL.SetFocus;
end;

procedure TLRUTSQL.SMSQLChange(Sender: TObject);
var
  toSearch: string;
begin
  LBFound.Visible := False;

  toSearch := SMSQL.GetWordAtRowCol(SMSQL.CaretXY);
  if Length(toSearch) > 0 then
    LBFound.Visible := SearchToLBFound(toSearch);

  if RightStr(SMSQL.Lines[SMSQL.CaretY - 1], 1) = '.' then
  begin
    toSearch := SMSQL.GetWordAtRowCol(Point(SMSQL.CaretX - 1, SMSQL.CaretY));
    if Length(toSearch) > 0 then
      LBFound.Visible := dotSearchToLBFound(toSearch);
  end;
end;

function TLRUTSQL.SearchToLBFound(toSearch: string): boolean;
var
  i: integer;
  posicion: TPoint;
  tmpStrings: TStringList;
begin
  LBFound.Clear;
  Result := False;

  // Añadimos los tokens
  for i := 1 to 16 do
    if not (strpos(StrUpper(PChar(autoTokens[i])), StrUpper(PChar(toSearch))) = nil) then
      LBFound.Items.Add(autoTokens[i]);

  // Añadimos las tablas de la Base de datos
  if LRUTDataModule.con.SQLConnection.Connected then
  begin
    tmpStrings := TStringList.Create;
    LRUTDataModule.md.GetTableNames(tmpStrings);
    for i := 0 to tmpStrings.Count - 1 do
      if not (strpos(StrUpper(PChar(tmpStrings[i])), StrUpper(PChar(toSearch))) =
        nil) then
        LBFound.Items.Add(tmpStrings[i]);
    FreeAndNil(tmpStrings);
  end;

  if LBFound.Items.Count > 0 then
  begin
    Result      := True;
    //TODO : Posicionar bien, como en Lazarus :)
    posicion    := SMSQL.CaretXY;
    LBFound.Left := posicion.x * SMSQL.CharWidth + 50;
    LBFound.Top := posicion.y * SMSQL.CharWidth + 40;
  end;
end;

function TLRUTSQL.DotSearchToLBFound(toSearch: string): boolean;
var
  tmpStrings: TStringList;
begin
  Result := False;

  if LRUTDataModule.con.SQLConnection.Connected then
  begin
    LBFound.Clear;
    tmpStrings := TStringList.Create;
    LRUTDataModule.md.GetFieldNames(toSearch, tmpStrings);
    LBFound.Items.AddStrings(tmpStrings);
    FreeAndNil(tmpStrings);
    if LBFound.Items.Count > 0 then
      Result := True;
  end;
end;

procedure TLRUTSQL.SMSQLKeyUp(Sender: TObject; var Key: word; Shift: TShiftState);
begin
  if key = VK_DOWN then
  begin
    LBFound.SetFocus;
    LBFound.Selected[0] := True;
  end;
end;

procedure TLRUTSQL.Translate(ALang: string);
var
  FallbackLang: string;
begin
  FallbackLang := ALang;

  if Alang = '' then
    GetLanguageIDs(ALang, FallbackLang);

  TranslateUnitResourceStrings('formprincipal', 'po' + DirectorySeparator +
    'lrut.' + ALang + '.po');
  LSQLDescription.Caption := rsDescripcion;
  BSaveSQL.Caption     := rsmGuarda;
  BEditorToGUI.Caption := 'Pasar SQL a Diseñador';
  BRunSQL.Caption      := rsEjecuta;
end;

procedure TLRUTSQL.LoadMenu;
begin
  if Menu = nil then
    exit;
  LoadMenu(Menu);
end;

procedure TLRUTSQL.LoadMenu(AMenu: TMenuItem);
var
  registro: TMenuItem;
  qTmp:     TDataSet;
begin
  Menu := AMenu;
  Menu.Clear;
  qTmp := LRUTManager.SQLs;
  qTmp.First;
  while not (qTmp.EOF) do
  begin
    registro := TMenuItem.Create(Menu);
    registro.Caption := qTmp.FieldByName('archivo').AsString;
    registro.OnClick := @LoadConf;
    Menu.Add(registro);
    qTmp.Next;
  end;
end;

procedure TLRUTSQL.LoadConf(Sender: TObject);
begin
  LoadConf(Sender, tmenuitem(Sender).Caption);
end;

procedure TLRUTSQL.LoadConf(Sender: TObject; ASQL: string);
begin
  if LRUTManager.LoadSQL(ASQL, LRUTDataModule.SQL) then
  begin
    LSQLDescriptionData.Caption := LRUTDataModule.sql.descripcion;
    SMSQL.Lines.LoadFromFile(LRUTDataModule.sql.archivo);
  end;

  if (LRUTDataModule.con.SQLConnection = nil) or
    (not LRUTDataModule.con.SQLConnection.Connected) then
    LoadConf(Sender, LRUTDataModule.sql.nconexion);

  if LRUTDataModule.con.SQLConnection <> nil then
    if LRUTDataModule.con.SQLConnection.Connected then
      BRunSQLClick(Sender);
end;

procedure TLRUTSQL.SQLToWorkZone;
begin
  LRUTManager.SQLToWorkZone := True;
end;

initialization

{$I frm_sql.lrs}

end.

