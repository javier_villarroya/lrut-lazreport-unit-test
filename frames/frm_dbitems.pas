{
 *****************************************************************************
 *                                                                           *
 *  frm_dbitems.pas, This file is part of the LazReport Unit Test (LRUT)     *
 *                                                                           *
 *  See the file gpl.txt, included in this distribution, for details         *
 *  about the copyright.                                                     *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
}

unit frm_dbitems;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, ComCtrls, Controls;

type

  { TLRUTDBItems }

  TLRUTDBItems = class(TFrame)
    ILApp:     TImageList;
    TVDBItems: TTreeView;
    procedure TVDBItemsClick(Sender: TObject);
    procedure TVDBItemsDblClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    procedure AddChildTVNodesFromStrings(node: TTreenode; tmp: TStringList);
  end;

implementation

uses
  datamodule, manager, dummy_workzone, dummy_fieldlist;

{ TLRUTDBItems }

procedure TLRUTDBItems.TVDBItemsClick(Sender: TObject);
var
  tmpFields: TStringList;
  tmpString: string;
begin
  if not (LRUTDataModule.con.SQLConnection.Connected) then
    exit;

  // Si hacemos click en una tabla añadimos los campos
  if (TVDBItems.Selected.Level = 0) and (TVDBItems.Selected.GetFirstChild = nil) then
  begin
    tmpFields := TStringList.Create;
    LRUTDataModule.md.GetFieldNames(TVDBItems.Selected.Text, tmpFields);
    AddChildTVNodesFromStrings(TVDBItems.Selected, tmpFields);
    FreeAndNil(tmpFields);
  end;
  // Si hacemos click en un campo añadimos la metadata
  if (TVDBItems.Selected.Level = 1) and (TVDBItems.Selected.GetFirstChild = nil) then
  begin
    tmpString := tmpString + LRUTDataModule.md.GetFieldTypeName(
      TVDBItems.Selected.Parent.Text, TVDBItems.Selected.Text);
    tmpString := tmpString + '(' + LRUTDataModule.md.GetFieldLength(
      TVDBItems.Selected.Parent.Text, TVDBItems.Selected.Text) + ')';
    TVDBItems.Items.AddChild(TVDBItems.Selected, tmpString);
  end;
end;

procedure TLRUTDBItems.TVDBItemsDblClick(Sender: TObject);
begin
  if not (LRUTDataModule.con.SQLConnection.Connected) then
    exit;
  if TVDBItems.Selected.Level = 0 then
    FFWorkZone.CreateTableGui(TVDBItems.Selected.Text, 10, 10);

  if TVDBItems.Selected.Level = 1 then
    FFFieldList.AddFieldNode(TVDBItems.Selected.Parent.Text,
      TVDBItems.Selected.Text, '', '', '', '');

  TVDBItems.Selected.Expand(False);
end;

procedure TLRUTDBItems.AddChildTVNodesFromStrings(node: TTreeNode; tmp: TStringList);
var
  i: integer;
begin
  for i := 0 to tmp.Count - 1 do
    if node = nil then
      TVDBItems.Items.AddChild(node, tmp[i]).ImageIndex := 0
    else
    if (LRUTDataModule.md.IsKeyField(node.Text, tmp[i])) then
      TVDBItems.Items.AddChild(node, tmp[i]).ImageIndex := 1
    else
      TVDBItems.Items.AddChild(node, tmp[i]);
end;

initialization

{$I frm_dbitems.lrs}

end.

