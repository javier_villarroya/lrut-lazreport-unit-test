{
 *****************************************************************************
 *                                                                           *
 *  main.pas, This file is part of the LazReport Unit Test (LRUT)            *
 *                                                                           *
 *  See the file gpl.txt, included in this distribution, for details         *
 *  about the copyright.                                                     *
 *                                                                           *
 *  This program is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                     *
 *                                                                           *
 *****************************************************************************
}

unit main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, Grids,
  ComCtrls, Menus, ExtCtrls, gettext, translations, LCLType, FileUtil,
  XMLPropStorage, LDockCtrl,

  //LRUT Units
  about, datamodule, manager, allforms,
  dummy_database, dummy_sql, dummy_report, dummy_grid, dummy_conf,
  dummy_dbitems, dummy_fieldlist, dummy_workzone;

type
  { TMForm }

  TMForm = class(TForm)
    mViewDBItems: TMenuItem;
    mViewFieldList: TMenuItem;
    mViewWorkZone: TMenuItem;
    mViewConfigurations: TMenuItem;
    mViewGrid: TMenuItem;
    mViewReport: TMenuItem;
    mViewSQL: TMenuItem;
    mViewConnection: TMenuItem;
    mView:    TMenuItem;
    PFieldList: TPanel;
    PWorkZone: TPanel;
    PConf:    TPanel;
    PGrid:    TPanel;
    PReport:  TPanel;
    PDatabase: TPanel;
    PSQL:     TPanel;
    PDBItems: TPanel;
    abreSQL:  TOpenDialog;
    mFile:    TMenuItem;
    mAcerca:  TMenuItem;
    mAyuda:   TMenuItem;
    mEn:      TMenuItem;
    mMultiWindow: TMenuItem;
    mEs:      TMenuItem;
    mSalir:   TMenuItem;
    mIdiomas: TMenuItem;
    SMVer:    TSplitter;
    SMHor0:   TSplitter;
    SMHor1:   TSplitter;
    SWZVer:   TSplitter;
    SWZHor:   TSplitter;
    XPSSession: TXMLPropStorage;
    mConnection: TMenuItem;
    MMMain:   TMainMenu;
    mReport:  TMenuItem;
    mSQL:     TMenuItem;
    PCMain:   TPageControl;
    PageControl2: TPageControl;
    tabPrincipal: TTabSheet;
    TSMain:   TTabSheet;
    TSSQLBuilder: TTabSheet;
    TSConfigurations: TTabSheet;
    tabSQLBuilder: TTabSheet;
    BarraDeEstado: TStatusBar;
    procedure btnVistaPreliminarClick(Sender: TObject);
    procedure desconectaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure mAcercaClick(Sender: TObject);
    procedure mAyudaClick(Sender: TObject);
    procedure mEnClick(Sender: TObject);
    procedure mEsClick(Sender: TObject);
    procedure mMultiWindowClick(Sender: TObject);
    procedure MIAddClick(Sender: TObject);
    procedure MIAggregateClick(Sender: TObject);
    procedure MIDeleteClick(Sender: TObject);
    procedure mSalirClick(Sender: TObject);
    procedure mViewConfigurationsClick(Sender: TObject);
    procedure mViewConnectionClick(Sender: TObject);
    procedure mViewDBItemsClick(Sender: TObject);
    procedure mViewFieldListClick(Sender: TObject);
    procedure mViewGridClick(Sender: TObject);
    procedure mViewReportClick(Sender: TObject);
    procedure mViewSQLClick(Sender: TObject);
    procedure mViewWorkZoneClick(Sender: TObject);
    procedure ShowSQLBuilder;
    procedure traduce(Lang: string);
  private
    //procedure JoinToGUI(TablaA, TablaB  : String ; PosicionA, PosicionB : Integer);
    procedure UpdateMenuState(Sender: TObject);
    procedure LoadMenus;
    procedure LoadTables;
    function FieldByColumn(ACol: integer): string;
    procedure SetupForms;
    procedure SetupFrames;
    procedure FormInit(var AForm: TForm; var AFrame: TFrame;
      AFile, ACaption: string; AMenu: TMenuItem);
  public
    DockingManager: TLazDockingManager;
    Initialized:    boolean;

    FormDataBase: TForm;
    FormSQL:      TForm;
    FormReport:   TForm;
    FormGrid:     TForm;
    FormConf:     TForm;
    FormWorkZone: TForm;
    FormFieldList: TForm;
    FormDBItems:  TForm;
  end;

var
  MForm: TMForm;

implementation

resourcestring
  rsLReports = 'Informes';
  rsNotTested = 'Not tested/implemented yet!';
  rsSQLBuilderEn = 'SQLBuilder en la BD : ';
  rsSQL      = 'SQL';
  rsTSMain   = 'Principal';
  rsTSSQLBuilder = 'SQLBuilder';
  rsTSConfigurations = 'Gestor de Configuraciones';
  rsmArchivo = 'Archivo';
  rsmIdioma  = 'Idiomas';
  rsmEs      = 'Castellano';
  rsmEn      = 'Ingles';
  rsmAyuda   = 'Ayuda';
  rsmPreferences = 'MultiVentana';
  rsmAcerca  = 'Acerca De';
  rsSalir    = 'Salir';
  rsmConexion = 'Base de Datos';

{ TMForm }

procedure TMForm.traduce(Lang: string);
var
  FallbackLang: string;
begin
  FFDatabase.Translate(Lang);
  FFSQL.Translate(Lang);
  FFReport.Translate(Lang);

  FallbackLang := Lang;

  if lang = '' then
    GetLanguageIDs(Lang, FallbackLang);

  TranslateUnitResourceStrings('formprincipal', 'po' + DirectorySeparator + 'lrut.%s.po',
    Lang, FallbackLang);

  //TabSheets
  TSMain.Caption := rsTSMain;
  TSSQLBuilder.Caption := rsTSSQLBuilder;
  TSConfigurations.Caption := rsTSConfigurations;

  // Menu
  mFile.Caption    := rsmArchivo;
  mIdiomas.Caption := rsmIdioma;
  mEs.Caption      := rsmEs;
  mEn.Caption      := rsmEn;
  mAyuda.Caption   := rsmAyuda;
  mMultiWindow.Caption := rsmPreferences;
  mAcerca.Caption  := rsmAcerca;
  mSalir.Caption   := rsSalir;
  mConnection.Caption := rsmConexion;
  mSQL.Caption     := rsSQL;
  mReport.Caption  := rsLReports;
end;

 //procedure TMForm.JoinToGUI(TablaA, TablaB  : String ; PosicionA, PosicionB : Integer);
 //var
 //  i : Integer;
 //  pA, pB : TPoint;
 //begin
 //  for i := 0 to SBWorkZone.ComponentCount-1 do
 //    begin
 //      if TablaA = SBWorkZone.Components[i].Name then
//        pa := Point(TTabla(SBWorkZone.Components[i]).Left , TTabla(SBWorkZone.Components[i]).Top*PosicionA);
//      if TablaB = SBWorkZone.Components[i].Name then
//        pb := Point(TTabla(SBWorkZone.Components[i]).Left , TTabla(SBWorkZone.Components[i]).Top*PosicionB);
//    end;

 //  DebugLn('A ' + TablaA + ' ' + IntToStr(pa.x) + '/' + IntToStr(pa.y));
 //  DebugLn('B ' + TablaB + ' ' + IntToStr(pb.x) + '/' + IntToStr(pb.y));

 //  SBWorkZone.Canvas.Line(pa, pb);
 //end;

procedure TMForm.btnVistaPreliminarClick(Sender: TObject);
begin
  LRUTDataModule.Informe.ShowReport;
end;

procedure TMForm.desconectaClick(Sender: TObject);
begin
  LRUTDataModule.rptQuery.Active := False;
  if (LRUTDataModule.con.SQLConnection.Connected) then
    LRUTDataModule.con.SQLConnection.Connected := False;
end;

procedure TMForm.FormActivate(Sender: TObject);
begin
  if Initialized then
    exit;
  Initialized := True;

  if mMultiWindow.Checked then
    begin
      SetupForms;
      mView.Visible := True;
    end
  else
    SetupFrames;

  if not (mMultiWindow.Checked) then
    exit;
end;

procedure TMForm.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  LRUTManager.SaveConfiguration(LRUTDataModule.acd + 'conf' + DirectorySeparator);
  if not (length(LRUTDataModule.con.ConnectionName) = 0) then
    desconectaClick(Sender);

  // NOTA TEMPORAL. como ahora el propietario de la conexion es nil
  //       todos aquellas sqlquery y sqltransactions que hagan referencia
  //       a la conexion mediante la propiedad database y que se hayan creado
  //       en el form designer, deben ser reajustados a nil manualmente, pues
  //       la conexion no ha sido creada en el designer y lleva un registro de
  //       que queries y que transacciones han sido enlazadas con ella.
  //       Como los componentes que no tiene owner como es el caso actual de
  //       la conexion, son normalmente liberados en el FormDestroy en ese punto
  //       los queries y transacciones diseñadas ya no son validas y provocan
  //       fallas al cerrar la aplicacion.
  //LRUTDataModule.qTransaction.DataBase := nil;
  //LRUTDataModule.rptQuery.DataBase     := nil;

  // Cerrando los Forms en MultiWindow
  if FormDataBase <> nil then
    FormDataBase.Close;
  if FormSQL <> nil then
    FormSQL.Close;
  if FormReport <> nil then
    FormReport.Close;
  if FormGrid <> nil then
    FormGrid.Close;
  if FormConf <> nil then
    FormConf.Close;
  if FormWorkZone <> nil then
    FormWorkZone.Close;
  if FormFieldList <> nil then
    FormFieldList.Close;
  if FormDBItems <> nil then
    FormDBItems.Close;
end;

procedure TMForm.FormCreate(Sender: TObject);
begin
  LRUTDataModule.acd := GetAppConfigDir(False);

  if not (DirectoryExists(LRUTDataModule.acd)) then
    CreateDir(LRUTDataModule.acd);
  if not (DirectoryExists(LRUTDataModule.acd + 'informes')) then
    CreateDir(LRUTDataModule.acd + 'informes');
  if not (DirectoryExists(LRUTDataModule.acd + 'sql')) then
    CreateDir(LRUTDataModule.acd + 'sql');
  if not (DirectoryExists(LRUTDataModule.acd + 'conf')) then
    CreateDir(LRUTDataModule.acd + 'conf');

  traduce('');

  LRUTManager.LoadConfiguration(LRUTDataModule.acd + 'conf' + DirectorySeparator);
  XPSSession.FileName := LRUTDataModule.acd + 'conf' + DirectorySeparator + 'lrut.xml';
  XPSSession.Active   := True;

  LoadMenus;
end;

procedure TMForm.FormDestroy(Sender: TObject);
begin
  FFWorkZone.CleanWorkZone;
end;

procedure TMForm.mAcercaClick(Sender: TObject);
begin
  AboutFrm.Show;
end;

procedure TMForm.mAyudaClick(Sender: TObject);
begin
  ShowMessage(rsNotTested);
end;

procedure TMForm.mEnClick(Sender: TObject);
begin
  traduce('en');
end;

procedure TMForm.mEsClick(Sender: TObject);
begin
  traduce('es');
end;

procedure TMForm.mMultiWindowClick(Sender: TObject);
begin
  ShowMessage('Para que tenga efecto este cambio deberas reiniciar la aplicacion');
end;

procedure TMForm.MIAddClick(Sender: TObject);
begin
  LRUTManager.SelectedFields.Append;
  LRUTDataModule.md.GetTableNames(FFFieldList.SGFieldsList.Columns[2].PickList);
end;

procedure TMForm.MIAggregateClick(Sender: TObject);
begin
  if FFFieldList.MIAggregate.Checked then
    FFFieldList.MIAggregate.Checked := False
  else
    FFFieldList.MIAggregate.Checked := True;
  FFFieldList.SGFieldsList.Columns[6].Visible := True;
end;

procedure TMForm.MIDeleteClick(Sender: TObject);
begin
  LRUTManager.SelectedFields.Delete;
end;

procedure TMForm.mSalirClick(Sender: TObject);
begin
  MForm.Close;
end;

procedure TMForm.mViewConfigurationsClick(Sender: TObject);
begin
  if mViewConfigurations.Checked then
    FormConf.Visible := True
  else
    FormConf.Visible := False;
end;

procedure TMForm.mViewConnectionClick(Sender: TObject);
begin
  if mViewConnection.Checked then
    FormDataBase.Visible := True
  else
    FormDataBase.Visible := False;
end;

procedure TMForm.mViewDBItemsClick(Sender: TObject);
begin
  if mViewDBItems.Checked then
    FormDBItems.Visible := True
  else
    FormDBItems.Visible := False;
end;

procedure TMForm.mViewFieldListClick(Sender: TObject);
begin
  if mViewFieldList.Checked then
    FormFieldList.Visible := True
  else
    FormFieldList.Visible := False;
end;

procedure TMForm.mViewGridClick(Sender: TObject);
begin
  if mViewGrid.Checked then
    FormGrid.Visible := True
  else
    FormGrid.Visible := False;
end;

procedure TMForm.mViewReportClick(Sender: TObject);
begin
  if mViewReport.Checked then
    FormReport.Visible := True
  else
    FormReport.Visible := False;
end;

procedure TMForm.mViewSQLClick(Sender: TObject);
begin
  if mViewSQL.Checked then
    FormSQL.Visible := True
  else
    FormSQL.Visible := False;
end;

procedure TMForm.mViewWorkZoneClick(Sender: TObject);
begin
  if mViewWorkZone.Checked then
    FormWorkZone.Visible := True
  else
    FormWorkZone.Visible := False;
end;

procedure TMForm.LoadMenus;
begin
  FFDatabase.LoadMenu(mConnection);
  FFSQL.LoadMenu(mSQL);
  FFReport.LoadMenu(mReport);
end;

procedure TMForm.LoadTables;
var
  tmpTables: TStringList;
begin
  if FFDBItems.TVDBItems.Items.Count = 0 then
  begin
    FFDBItems.TVDBItems.Items.Clear;
    tmpTables := TStringList.Create;
    LRUTDataModule.md.GetTableNames(tmpTables);
    FFDBItems.AddChildTVNodesFromStrings(nil, tmpTables);
    tmpTables.Free;
  end;
end;

function TMForm.FieldByColumn(ACol: integer): string;
var
  c: TGridColumn;
begin
  // TODO : Revisar jesus
  C      := FFFieldList.SGFieldsList.Columns[ACol];
  Result := C.Title.Caption;
end;

procedure TMForm.SetupForms;
var
  ConfPath: string;
begin
  DockingManager := TLazDockingManager.Create(Self);

  ConfPath := LRUTDataModule.acd + 'conf' + DirectorySeparator;
  FormInit(FormDataBase, TFrame(FFDataBase), ConfPath + 'database.xml',
    'Database', mViewConnection);
  FFDataBase.SBStatus := BarraDeEstado;

  FormInit(FormSQL, TFrame(FFSQL), ConfPath + 'sql.xml', 'SQL', mViewSQL);
  FFSQL.SBStatus := BarraDeEstado;

  FormInit(FormReport, TFrame(FFReport), ConfPath + 'report.xml', 'Report', mViewReport);
  FFReport.SBStatus := BarraDeEstado;

  FormInit(FormGrid, TFrame(FFGrid), ConfPath + 'grid.xml', 'Grid', mViewGrid);
  FormInit(FormConf, TFrame(FFConf), ConfPath + 'conf.xml', 'Configuration',
    mViewConfigurations);
  FormInit(FormWorkZone, TFrame(FFWorkZone), ConfPath + 'workzone.xml',
    'WorkZone', mViewWorkZone);
  FormInit(FormFieldList, TFrame(FFFieldList), ConfPath + 'fieldlist.xml',
    'Field List', mViewFieldList);
  FormInit(FormDBItems, TFrame(FFDBItems), ConfPath + 'dbitems.xml',
    'DB Items', mViewDBItems);


  PCMain.Pages[0].Free;
end;

procedure TMForm.SetupFrames;
begin
  FFDataBase.Parent := PDataBase;
  FFDataBase.SBStatus := BarraDeEstado;
  FFSQL.Parent      := PSQL;
  FFSQL.SBStatus    := BarraDeEstado;
  FFReport.Parent   := PReport;
  FFReport.SBStatus := BarraDeEstado;
  FFGrid.Parent     := PGrid;
  FFConf.Parent     := PConf;
  FFWorkZone.Parent := PWorkZone;
  FFFieldList.Parent := PFieldList;
  FFDBItems.Parent  := PDBItems;

  PCMain.ActivePage := TSMain;
end;

procedure TMForm.ShowSQLBuilder;
begin
  BarraDeEstado.SimpleText := rsSQLBuilderEn + LRUTDataModule.con.DatabaseName;

  // Añadimos las tablas
  LoadTables;

  if LRUTManager.SQLToWorkZone then
    FFWorkZone.SQLToWorkZone;
end;

procedure TMForm.FormInit(var AForm: TForm; var AFrame: TFrame;
  AFile, ACaption: string; AMenu: TMenuItem);
begin
  AForm      := TAllForms.Create(MForm);
  AForm.Name := AFrame.Name;
  AForm.Caption := ACaption;
  //TODO:  AForm.OnClose:=@UpdateMenuState;
  if not (FileExists(AFile)) then
  begin
    AForm.Height := AFrame.Height;
    AForm.Width  := AFRame.Width;
  end;
  TAllForms(AForm).PropStorage.FileName := AFile;
  AFrame.Parent := AForm;

  if AMenu.Checked then
    AForm.Show;
end;

procedure TMForm.UpdateMenuState(Sender: TObject);
begin
  TMenuItem(Sender).Checked := False;
end;

initialization
  {$I main.lrs}

end.

