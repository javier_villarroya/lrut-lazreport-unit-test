http://lrut.alasombra.net

LRUT es el acronimo de LazReport Unit Test y es un proyecto que intenta ser una ayuda 
para el dise�o rapido de informes basados en una consulta SQL.

Paquetes requeridos:
SQLDBLaz, lazdbexport, lazreport, lazreportpdfexport : incluidos en Lazarus
LazgaSQLParser : svn://alasombra.net/lazgasqlparser
PowerPDF : https://lazarus-ccr.svn.sourceforge.net/svnroot/lazarus-ccr/components/powerpdf
