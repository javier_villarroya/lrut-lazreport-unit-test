
# hash value = 81172163
formprincipal.rslreports='Informes'


# hash value = 80421489
formprincipal.rsnottested='Not tested/implemented yet!'


# hash value = 105623074
formprincipal.rsconecta='Conectar'


# hash value = 100754210
formprincipal.rsdesconecta='Desconectar'


# hash value = 79929486
formprincipal.rsintroducenconexion='Introduce un nombre para la configura'+
'cion'


# hash value = 244828192
formprincipal.rssqlbuilderen='SQLBuilder en la BD : '


# hash value = 210593340
formprincipal.rsintroducensql='Introduce una descripcion sobre la SQL'


# hash value = 1457615
formprincipal.rsdesconectado='Desconectado'


# hash value = 4818031
formprincipal.rscampo='Campo'


# hash value = 5929249
formprincipal.rstabla='Tabla'


# hash value = 5671614
formprincipal.rsorden='Orden'


# hash value = 80755599
formprincipal.rsfiltro='Filtro'


# hash value = 193510810
formprincipal.rsusuario='Usuario :'


# hash value = 182709914
formprincipal.rspassword='Password :'


# hash value = 83273274
formprincipal.rshost='Host :'


# hash value = 71139626
formprincipal.rstipobbdd='Tipo BBDD :'


# hash value = 73827898
formprincipal.rsbbdd='BBDD :'


# hash value = 104432234
formprincipal.rsnombre='Nombre :'


# hash value = 428132
formprincipal.rsguardapass='Guardar Password'


# hash value = 22620
formprincipal.rssql='SQL'


# hash value = 207096186
formprincipal.rsdescripcion='Descripcion :'


# hash value = 12242498
formprincipal.rsejecuta='Ejecutar'


# hash value = 5618895
formprincipal.rsnew='Nuevo'


# hash value = 79366786
formprincipal.rsedit='Editar'


# hash value = 148073634
formprincipal.rsvistaprevia='Vista Preliminar'


# hash value = 75040514
formprincipal.rsprint='Imprimir'


# hash value = 227460
formprincipal.rsimprimirgrid='Imprimir Grid'


# hash value = 261502485
formprincipal.rsconectadoa='Conectado a %s Correctamente'


# hash value = 195338433
formprincipal.rsmuybien=#194#161'Muy bien!'


# hash value = 120981935
formprincipal.rsconcorrecta='La conexion ha sido correcta %s '#194#191'Qu'+
'ieres guardar la configuracion?'


# hash value = 328911
formprincipal.rsinfo='Info'


# hash value = 76945411
formprincipal.rsnosehapodido='No se ha podido conectar a %s'


# hash value = 165234591
formprincipal.rsquieresborrar=#194#191'Quieres borrar la conexion %s ?'


# hash value = 14029203
formprincipal.rsinforgenerado='Se ha generado el nuevo informe %s'


# hash value = 145975391
formprincipal.rsnoinfoinfor='No existe informacion del Informe%s'#194#191+
'vamos a insertarlo?'


# hash value = 212565455
formprincipal.rsnoinfosql='No existe informacion del SQL%s'#194#191'vamos'+
' a insertarlo?'


# hash value = 202768623
formprincipal.rsborrarsql=#194#191'Quieres borrar la SQL %s ?'


# hash value = 43653455
formprincipal.rsdebesconectar='Deberias conectarte a alguna BBDD primero'


# hash value = 232330783
formprincipal.rsborrarinforme=#194#191'Quieres borrar informe %s ?'


# hash value = 261774063
formprincipal.rssqlnovalido='SQL no valido'


# hash value = 236716389
formprincipal.rsdescripcioninforme='Introduce una descripcion para el inf'+
'orme'


# hash value = 185977542
formprincipal.rsexportarapdf='Exportar a PDF'


# hash value = 220120386
formprincipal.rsbguitoeditor='Pasar Dise'#195#177'o a Editor'


# hash value = 4870636
formprincipal.rstsmain='Principal'


# hash value = 163294402
formprincipal.rstssqlbuilder='SQLBuilder'


# hash value = 138397731
formprincipal.rstsconfigurations='Gestor de Configuraciones'


# hash value = 216859267
formprincipal.rslconnections='Conexiones'


# hash value = 12771762
formprincipal.rslinfo='Doble click sobre el registro a borrar'


# hash value = 221352447
formprincipal.rsrecursivo='Borrado recursivo'


# hash value = 143257743
formprincipal.rsmarchivo='Archivo'


# hash value = 263218115
formprincipal.rsmidioma='Idiomas'


# hash value = 180863215
formprincipal.rsmes='Castellano'


# hash value = 84206275
formprincipal.rsmen='Ingles'


# hash value = 4787105
formprincipal.rsmayuda='Ayuda'


# hash value = 246975170
formprincipal.rsmguarda='Guardar'


# hash value = 210334517
formprincipal.rsmacerca='Acerca De'


# hash value = 5866242
formprincipal.rssalir='Salir'


# hash value = 264502659
formprincipal.rsmconexion='Base de Datos'

